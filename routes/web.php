<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','Auth\LoginController@index');


Route::middleware(['auth'])->group(function () { 

    Route::get('/dashboard', 'DashboardController@index');
   
     
   
    
    Route::get('profil/{id}', 'PenggunaController@show');
    Route::get('profil/edit/{id}', 'PenggunaController@edit');
    Route::put('profil/update/{id}', 'PenggunaController@update');
    Route::get('profil/admin/edit/{id}', 'PenggunaController@edit');
    Route::put('profil/admin/update/{id}', 'PenggunaController@update');
    
    // Route::get('detail/{id}', 'LaporanController@show');
    Route::get('edit/{id}', 'LaporanController@edit');
    Route::get('logout', 'Auth\LoginController@logout');
    Route::get('/forgot_password', 'Auth\ForgotPasswordController@logout');

    // Menampilkan detail laporan
    Route::get('detail-laporan/{id}', 'LaporanController@show')->name('detail-laporan');
   
    // CRUD Fitur
    Route::post('tambah-fitur', 'FiturController@create');
    Route::post('hapus-fitur/{id}', 'FiturController@delete');
    Route::post('update-fitur/{id}', 'FiturController@update');
   
    // User Requirement
    Route::post('tambah-requirement/{id}', 'RequirementController@update'); //menambahkan dan mengupdate tanggal, kesimpulan
    Route::post('requirement-kirim/{id}', 'RequirementController@send');
    Route::get('generate/requirement/{id}', 'RequirementController@generate');

    // Pengembangan
    Route::post('tambah-fitur-pengembangan', 'PengembanganController@create');
    Route::post('hapus-fitur-pengembangan/{id}', 'PengembanganController@delete');
    Route::post('update-fitur-pengembangan/{id}', 'PengembanganController@updatefitur');
    Route::post('tambah-pengembangan/{id}', 'PengembanganController@update'); //menambahkan dan mengupdate tanggal, kesimpulan
    Route::post('pengembangan-kirim/{id}', 'PengembanganController@send');
    Route::get('generate/pengembangan/{id}', 'PengembanganController@generate');

    // Testing
    Route::post('tambah-fitur-testing', 'TestingController@create');
    Route::post('hapus-fitur-testing/{id}', 'TestingController@delete');
    Route::post('update-fitur-testing/{id}', 'TestingController@updatefitur');
    Route::post('tambah-testing/{id}', 'TestingController@update'); //menambahkan dan mengupdate tanggal, kesimpulan
    Route::post('testing-kirim/{id}', 'TestingController@send');
    Route::get('generate/testing/{id}', 'TestingController@generate');

    // Rilis
    Route::post('tambah-fitur-rilis', 'RilisController@create');
    Route::post('hapus-fitur-rilis/{id}', 'RilisController@delete');
    Route::post('update-fitur-rilis/{id}', 'RilisController@updatefitur');
    Route::post('tambah-rilis/{id}', 'RilisController@update'); //menambahkan dan mengupdate tanggal, kesimpulan
    Route::post('rilis-kirim/{id}', 'RilisController@send');
    Route::get('generate/rilis/{id}', 'RilisController@generate');
    
    


    Route::middleware(['admin'])->group(function(){
        
        Route::get('tambah-pengguna', 'PenggunaController@index');
        Route::post('tambah-pengguna', 'PenggunaController@create');
        Route::get('hapus-pengguna/{id}', 'PenggunaController@delete');
        Route::get('aktif-pengguna/{id}', 'PenggunaController@restore');
        Route::get('/unitkerja', 'Admin\UnitkerjaController@index');
        Route::post('tambah-unitkerja', 'Admin\UnitkerjaController@create');
        Route::delete('hapus-unitkerja/{id}', 'Admin\UnitkerjaController@delete');
        Route::put('update-unitkerja/{id}', 'Admin\UnitkerjaController@update');
        
        
    });
    Route::middleware(['subkoordinator'])->group(function(){

        Route::put('update-laporan/{id}', 'LaporanController@update');
        Route::put('subkoordinator/kirim/{id}', 'Subkoordinator\UpdateController@kirim');
        Route::delete('hapus-laporan/{id}', 'LaporanController@delete');
        Route::get('create', 'LaporanController@index');
        Route::post('tambah-laporan', 'LaporanController@create');
       
       
        //Subkoordinator
        // Route::get('membuat-laporan', 'Subkoordinator\LaporanController@index')->name('laporan-index');
        // Route::post('membuat-laporan', 'Subkoordinator\LaporanController@create');
        // Route::get('mengubah-subkoordinator/{id}/edit', 'Subkoordinator\LaporanController@edit');
        // Route::put('mengubah-subkoordinator/{id}/update', 'Subkoordinator\LaporanController@update');
        // Route::delete('hapus-laporan/{id}', 'Subkoordinator\LaporanController@delete');
        // Route::get('detail-subkoordinator/{id}', 'Subkoordinator\LaporanController@show');
   
    });

    Route::middleware(['koordinator'])->group(function(){
        Route::put('koordinator/update/{id}', 'Koordinator\UpdateController@update');
        Route::put('koordinator/kirim/{id}', 'Koordinator\UpdateController@kirim');
        Route::put('koordinator/revisi/{id}', 'Koordinator\UpdateController@revisi');
      
       
       

         //Koordinator
        //  Route::get('mengubah-laporan/{id}/edit', 'Koordinator\LaporanController@edit');
        //  Route::put('mengubah-laporan/{id}/update', 'Koordinator\LaporanController@update');
        //  Route::delete('hapus-laporan/{id}', 'Koordinator\LaporanController@delete');
        //  Route::get('detail/{id}', 'Koordinator\LaporanController@show');
    });

    Route::middleware(['direktur'])->group(function(){
        Route::put('direktur/update/{id}', 'Direktur\UpdateController@update');
        Route::put('direktur/kirim/{id}', 'Direktur\UpdateController@kirim');
        Route::put('direktur/revisi/{id}', 'Direktur\UpdateController@revisi');
        
       

        //Direktur
        // Route::get('mengubah-laporan/{id}/edit', 'Direktur\LaporanController@edit');
        // Route::put('mengubah-laporan/{id}/update', 'Direktur\LaporanController@update');
        // Route::delete('hapus-laporan/{id}', 'Direktur\LaporanController@delete');
        // Route::get('detail/{id}', 'Direktur\LaporanController@show');
    });

    
});










Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');