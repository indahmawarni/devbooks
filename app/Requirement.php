<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requirement extends Model
{
    public $fillable = [
        'nama_fitur', 'status_requirement'
    ];
    public function laporan() {
        return $this->belongsTo(Laporan::class);
    }
    public function subkoordinator() {
        return $this->hasOne(User::class, 'id', 'subkoordinator_id');
    }
    public function koordinator() {
        return $this->hasOne(User::class, 'id', 'koordinator_id');
    }
    public function direktur() {
        return $this->hasOne(User::class, 'id', 'direktur_id');
    }
}
