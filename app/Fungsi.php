<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fungsi extends Model
{
    public $fillable = [
        'namafungsi',
        'check'
    ];
    public function laporan() {
        return $this->belongsTo(Laporan::class);
    }
    // public function tahapan() {
    //     return $this->belongsTo(Tahapan::class);
    // }
    public function tahapan() {
        return $this->hasOne(Laporan::class);
    }
    

    public $timestamps = false;
}
