<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sandi extends Model
{
    protected $fillable = [
        'password',
    ];

    public function user() {
        return $this->belongsTo(User::class);
        
    }
}

