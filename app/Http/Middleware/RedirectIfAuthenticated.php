<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/dashboard');
        }
        return $next($request);
        // if (Auth::guard($guard)->check()) {
        //     if(Auth::attempt(['username' => $request->username, 'password' => $request->password]))
        //         {
        //             return redirect('/dashboard');
        //         }
        //     else 
        //         {
        //         return redirect('/')->with('message', 'Username atau Password Salah');
        //         }
        // }
        
        
    }
}
