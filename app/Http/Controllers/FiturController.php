<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Laporan;
use App\Requirement;
use App\Pengembangan;
use App\Testing;
use App\Rilis;
use App\Tahapan;
use Auth;

class FiturController extends Controller
{
    public function create(Request $request)
    {
        $requirement = new Tahapan;
        $role_id = Auth::user()->role_id;
        $nama_fitur = $request->nama_fitur;
        $laporan_id = $request->laporan_id;
        $status_requirement = $request->status_requirement;
        foreach ($nama_fitur as $nf) {
            $data[] = ['nama_fitur' => $nf, 'laporan_id' => $laporan_id, 'status_requirement' => $status_requirement, 'role_id' => $role_id ];
        }
        $requirement->insert($data);
        // $pengembangan->insert($data);
        // $testing->insert($data);
        // $rilis->insert($data);
        
        
        return redirect()->route('detail-laporan',['id'=>$laporan_id])->with('success', 'Data berhasil ditambahkan');
    }

    public function delete(Request $request, $id)
    {
        $laporan_id = $request->laporan_id;
        $fitur =   Tahapan::findOrFail($id);
        $fitur->delete();         
    
        return redirect()->route('detail-laporan',['id'=>$laporan_id])->with('success', 'Data berhasil dihapus');        
    }

    public function update(Request $request, $id)
    {
       
        $nama_fitur = $request->nama_fitur;
        $laporan_id = $request->laporan_id;
        $fitur =   Tahapan::findOrFail($id);
        $fitur->nama_fitur = $nama_fitur;
        $fitur->save();   
        return redirect()->route('detail-laporan',['id'=>$laporan_id])->with('success', 'Data berhasil diperbarui');
    }
    
        

}

