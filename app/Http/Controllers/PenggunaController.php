<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\Sandi;
use App\Unitkerja;
use Auth;
use Validator;


class PenggunaController extends Controller
{
    public function index()
    {   
        $user = User::all();
        $roles = Role::where('id','!=',1)->get();
        $unitkerja = UnitKerja::all(); 
        
        return view('admin.tambahpengguna', compact('user','roles','unitkerja'));
    }
    public function create(Request $request) 
    {
        $request->validate([
            'nama'      => 'required',
            'username'  => 'required|unique:users|max:20|alpha_dash',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:8',
            'role'      => 'required',
            'unitkerja' => 'required',
        ], [
            'nama.required'          => 'Nama tidak boleh kosong',
            // 'nama.alpha'             => 'Nama harus menggunakan alfabet',
            'username.required'      => 'Username tidak boleh kosong',
            'email.required'         => 'Email tidak boleh kosong',
            'password.required'      => 'Password tidak boleh kosong',
            'role.required'          => 'Role harus dipilih',
            'unitkerja.required'     => 'Unit Kerja harus dipilih',
            'password.min'           => 'Password minimal diisi dengan 8 karakter',
            'email.email'            => 'Email tidak valid',
            'email.unique'           => 'Email sudah terdaftar',
            'username.unique'        => 'Username sudah terdaftar',
            'username.alpha_dash'    => 'Karakter tidak valid',
        ]);    

        $user = new User;
        $user->nama = $request->nama;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role_id = $request->role;
        $user->remember_token = \Str::random(60);
        $user->unitkerja_id = $request->unitkerja;
        $user->save();
        $user = User::find($user->id);
        $user->sandi()->create([
            "password" => $request->password
            ]);

        return redirect('/dashboard')->with('success','Data berhasil ditambahkan');
    }
    public function delete($id) {
        $user = User::find($id);
        $user->delete();
        return redirect('/dashboard')->with('success','Pengguna berhasil dinonaktifkan');
    }

    public function restore($id) {
        $user = User::onlyTrashed()->where('id',$id);
        $user->restore();
        return redirect('/dashboard')->with('success','Pengguna berhasil diaktifkan');
    }

    public function show ($id)
    {
        $user = User::findOrFail($id);
        $unitkerja = UnitKerja::all();
        return view('user.detailprofil', compact('user', 'unitkerja'));

    }

    // public function edit($id) 
    // {
    //     $user = User::findOrFail($id);
    //     $roles = Role::where('id','!=',1)->get();
    //     $unitkerja = Unitkerja::all();
    //     return view('user.editprofil', compact('user','roles','unitkerja'));
        
    //     // return view('user.editprofil',['user' => $user, 'roles' => $roles, 'unitkerja' => $unitkerja]);
    // }

    public function edit($id) 
    {
        if (Auth::user()->role_id == 1) {
            $user = User::findOrFail($id);
            $roles = Role::where('id','!=',1)->get();
            $unitkerja = Unitkerja::all();
            // return view('admin.editprofil', compact('user','roles','unitkerja'));
            return view('admin.editprofil',['user' => $user, 'roles' => $roles, 'unitkerja' => $unitkerja]);
        }
        elseif (Auth::user()->role_id !== 1) {
            $user = User::findOrFail($id);
            $roles = Role::where('id','!=',1)->get();
            $unitkerja = Unitkerja::all();
            return view('user.editprofil', compact('user','roles','unitkerja'));
        
        }

        
        // return view('user.editprofil',['user' => $user, 'roles' => $roles, 'unitkerja' => $unitkerja]);
    }

    public function update(Request $request, $id) 
    {
        $user = User::findOrFail($id);
        $request->validate([
            'nama'      => 'required',
            'username'  => 'required|max:20|alpha_dash|unique:users,username,'.$user->id,
            'email'     => 'required|email|unique:users,email,'.$user->id,
            'password'  => 'required|min:5',
            'role'      => 'required',
            'unitkerja' => 'required',
        
        ]); 
        if (Auth::user()->role_id == 1) {
            User::findorFail($id)->update(
                [   
                    $user = User::findorFail($id),
                    $user->nama = $request->nama,
                    $user->username = $request->username,
                    $user->email = $request->email,
                    $user->password = bcrypt($request->password),
                    $user->role_id = $request->role,
                    $user->remember_token = \Str::random(60),
                    $user->unitkerja_id = $request->unitkerja,
                    $user->save(),
                    $user->sandi()->update([
                        "password" => $request->password
                        ]),
                    $user->save(),
                ] 
                );
                return redirect('/dashboard')->with('success','Data berhasil diperbarui');
        }
        elseif (Auth::user()->role_id !== 1) {
            User::findorFail($id)->update(
                [   
                    $user = User::findorFail($id),
                    $user->nama = $request->nama,
                    $user->username = $request->username,
                    $user->email = $request->email,
                    $user->password = bcrypt($request->password),
                    // $user->role_id = $request->role,
                    $user->remember_token = \Str::random(60),
                    // $user->unitkerja_id = $request->unitkerja,
                    $user->save(),
                    $user->sandi()->update([
                        "password" => $request->password
                        ]),
                    $user->save(),
                ] 
                );
                return redirect('profil/'.Auth::user()->id)->with('success','Data berhasil diperbarui');
        }


    }

    





}
