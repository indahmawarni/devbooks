<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Laporan;
use App\Requirement;
use App\Pengembangan;
use App\Testing;
use App\Rilis;
use App\Tahapan;
use Auth;
use PDF;

class PengembanganController extends Controller
{
    // public function create(Request $request)
    // {
    //     $pengembangan = new Pengembangan;
    //     $testing = new Testing;
    //     $rilis = new Rilis;
    //     $nama_fitur = $request->nama_fitur;
    //     $laporan_id = $request->laporan_id;
    //     foreach ($nama_fitur as $nf) {
    //         $data[] = ['nama_fitur' => $nf, 'laporan_id' => $laporan_id,];
    //     }
    //     $pengembangan->insert($data);
    //     $testing->insert($data);
    //     $rilis->insert($data);

    //     return redirect()->route('detail-laporan',['id'=>$laporan_id])->with('success', 'Data berhasil ditambahkan');
    // }

    // public function delete(Request $request, $id)
    // {
    //     $laporan_id = $request->laporan_id;
    //     $pengembangan=   Pengembangan::findOrFail($id);
    //     $testing     =   Testing::findOrFail($id);
    //     $rilis       =   Rilis::findOrFail($id);          
    //     $pengembangan->delete();
    //     $testing->delete();
    //     $rilis->delete();        
    
    //     return redirect()->route('detail-laporan',['id'=>$laporan_id])->with('success', 'Data berhasil dihapus');        
    // }

    // public function updatefitur(Request $request, $id)
    // {
    //     $nama_fitur = $request->nama_fitur;
    //     $laporan_id = $request->laporan_id;
    //     $pengembangan=   Pengembangan::findOrFail($id);
    //     $testing     =   Testing::findOrFail($id);
    //     $rilis       =   Rilis::findOrFail($id);
    //     $pengembangan->nama_fitur = $nama_fitur;
    //     $rilis->nama_fitur = $nama_fitur;
    //     $testing->nama_fitur = $nama_fitur;
    //     $pengembangan->save();
    //     $testing->save();
    //     $rilis->save();
   
    //     return redirect()->route('detail-laporan',['id'=>$laporan_id])->with('success', 'Data berhasil diperbarui');
    // }

    public function update(Request $request, $id)
    {
        $tgl_pengembangan = $request->tgl_pengembangan;
        $kesimpulan_pengembangan = $request->kesimpulan_pengembangan;

        Laporan::where('id', $id)->update(
            [
                'tgl_pengembangan' => $tgl_pengembangan,
                'kesimpulan_pengembangan' => $kesimpulan_pengembangan
            ]
        );
        return redirect()->route('detail-laporan', ['id' => $id])->with('success', 'Tanggal dan kesimpulan berhasil ditambahkan');
    }

    public function send(Request $r, $id){  
              
        if (Auth::user()->role_id == 2) {
            $laporan = Laporan::where('id', $id)->first();
           $tgl_pengembangan = $r->input('tgl_pengembangan');
           $kesimpulan_pengembangan = $r->input('kesimpulan_pengembangan');
        Laporan::where('id', $id)->update([

                'status' => 'Diajukan',
                'posisi' => 'Koordinator',
                'tgl_pengembangan' => $tgl_pengembangan,
                'kesimpulan_pengembangan' => $kesimpulan_pengembangan,
            ]);
            // $requirement = Requirement::where('laporan_id', $id)->where('status_requirement', 1)->get();
            $pengembangan = Tahapan::where('laporan_id', $id)->get();
            // $pengembangan = Tahapan::where([
            //     'laporan_id' => $id,
            //     'status_requirement' => 1
            // ])->get();
            foreach ($pengembangan as $rq) {
                if (isset($r->catatan_pengembangan[$rq->id])){
                    $catatan_pengembangan = $r->catatan_pengembangan[$rq->id];
                }
                else {
                    $catatan_pengembangan = '';
                }
                if (isset($r->status_pengembangan[$rq->id])){
                    $status_pengembangan = 1;
                }
                else {
                    $status_pengembangan = 0;
                }
                Tahapan::where('id', $rq->id)->update([
                    'catatan_pengembangan' => $catatan_pengembangan,
                    'status_pengembangan' => $status_pengembangan,
                    'status_testing' => $status_pengembangan,

                ]);
            } 
            
           
            return redirect()->route('detail-laporan',['id'=>$id])->with('success', 'Data berhasil dikirim');
        }
        elseif (Auth::user()->role_id == 3) {
            $laporan = Laporan::where('id', $id)->first();
            $tgl_pengembangan = $r->input('tgl_pengembangan');
            $kesimpulan_pengembangan = $r->input('kesimpulan_pengembangan');
            Laporan::where('id', $id)->update([

                'status' => 'Diajukan',
                'posisi' => 'Direktur',
                'tgl_pengembangan' => $tgl_pengembangan,
                'kesimpulan_pengembangan' => $kesimpulan_pengembangan,
            ]);
            $pengembangan = Tahapan::where('laporan_id', $id)->get();
            foreach ($pengembangan as $rq) {
                if (isset($r->catatan_pengembangan[$rq->id])){
                    $catatan_pengembangan = $r->catatan_pengembangan[$rq->id];
                }
                else {
                    $catatan_pengembangan = '';
                }
                if (isset($r->status_pengembangan[$rq->id])){
                    $status_pengembangan = 1;
                }
                else {
                    $status_pengembangan = 0;
                }
                Tahapan::where('id', $rq->id)->update([
                    'catatan_pengembangan' => $catatan_pengembangan,
                    'status_pengembangan' => $status_pengembangan,
                    'status_testing' => $status_pengembangan,
                ]);
            } 
            return redirect()->route('detail-laporan',['id'=>$id])->with('success', 'Data berhasil dikirim');
        }
        elseif (Auth::user()->role_id == 4) {
            $laporan = Laporan::where('id', $id)->first();
            $tgl_pengembangan = $r->input('tgl_pengembangan');
            $kesimpulan_pengembangan = $r->input('kesimpulan_pengembangan');
            Laporan::where('id', $id)->update([

                'status' => 'Persiapan',
                'posisi' => 'Subkoordinator',
                'nama_tahapan' => 'Testing',
                'tgl_pengembangan' => $tgl_pengembangan,
                'kesimpulan_pengembangan' => $kesimpulan_pengembangan,
            ]);
            $pengembangan = Tahapan::where('laporan_id', $id)->get();
            foreach ($pengembangan as $rq) {
                if (isset($r->catatan_pengembangan[$rq->id])){
                    $catatan_pengembangan = $r->catatan_pengembangan[$rq->id];
                }
                else {
                    $catatan_pengembangan = '';
                }
                if (isset($r->status_pengembangan[$rq->id])){
                    $status_pengembangan = 1;
                }
                else {
                    $status_pengembangan = 0;
                }
                Tahapan::where('id', $rq->id)->update([
                    'catatan_pengembangan' => $catatan_pengembangan,
                    'status_pengembangan' => $status_pengembangan,
                    'status_testing' => $status_pengembangan,
                ]);
            } 
            return redirect()->route('detail-laporan',['id'=>$id])->with('success', 'Data berhasil disetujui');
        }      
    }

    public function generate($id)
    {
        $laporan = Laporan::findOrFail($id);
        $pengembangan = Tahapan::where('laporan_id', $id)->where('status_pengembangan', 1)->get();
        $pdf = PDF::loadview('user.generate_pengembangan', ['laporan' => $laporan, 'pengembangan' => $pengembangan]);
        return $pdf->stream();
    }
}
