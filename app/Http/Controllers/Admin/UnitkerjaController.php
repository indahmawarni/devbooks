<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Unitkerja;
use App\User;
use Auth;



class UnitkerjaController extends Controller
{
    public function index()
    {   
        $unitkerja = Unitkerja::all();
        return view('admin.unitkerja')->with(['unitkerja' => $unitkerja]);
    }

    public function create(Request $request) {
        
        $unitkerja = new Unitkerja;
        $unitkerja->nama = $request->nama;
        $unitkerja->save();
        return redirect('/unitkerja')->with('success','Data berhasil ditambahkan');
    }

    public function delete($id)
    {
        $unitkerja = Unitkerja::findOrFail($id);
        $unitkerja->delete();
        return redirect('/unitkerja')->with('success','Data berhasil dihapus');
    }


    // public function edit($id) {
       
       
    //     $unitkerja = Unitkerja::findorFail($id);

    //     return view('user.editprofil', compact('user','roles','unitkerja'));
        
    //     // return view('user.editprofil',['user' => $user, 'roles' => $roles, 'unitkerja' => $unitkerja]);
    // }

    public function update(Request $request, $id) 
    {
        $unitkerja = Unitkerja::findorFail($id);
        $unitkerja->update($request->all());
        return redirect('/unitkerja')->with('success','Data berhasil diperbarui');
    }

    public function show()
    {   
        return view('admin.tambahdirektorat');
    }





}
