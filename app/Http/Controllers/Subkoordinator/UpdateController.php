<?php

namespace App\Http\Controllers\Subkoordinator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Laporan;
use App\Tahapan;
use DB;
use Auth;
use App\User;
use App\Role;


class UpdateController extends Controller
{
   
    public function update(Request $request, $id) 
    {

        Laporan::find($id)->update(
            [
            $userId = Auth::id(),
            $user = User::find($userId),
            $laporan = Laporan::find($id),
            $laporan->tgl_laporan = $request->tgl_laporan,
            $laporan->versi = $request->versi,
            $laporan->judul = $request->judul,
            $laporan->perihal = $request->perihal,
            $laporan->jenis = $request->jenis,
            $laporan->isi = $request->isi,
            // $laporan->tahapan_id = $request->id_tahapan,
            $laporan->kesimpulan = $request->kesimpulan,
            $laporan->subkoordinator_id = $userId,
            $laporan->posisi = 'Subkoordinator' ,
            $laporan->status = 'Persiapan',
            $laporan->save(),
            ]
        );
        
        return redirect('/dashboard')->with('success','Data berhasil diperbarui');
    }

    public function kirim(Request $request, $id) 
    {
        Laporan::find($id)->update(
            [
           
            $laporan = Laporan::find($id),
            $laporan->posisi = 'Koordinator' ,
            $laporan->status = 'Diajukan',
            $laporan->save(),
            ]
        );
        
        return redirect('/dashboard')->with('success','Data berhasil dikirim');
    }


    // public function delete($id)
    // {
        
    //         $laporan = Laporan::find($id);
    //         $laporan->delete();
    //         return redirect('/dashboard')->with('sukses','Data berhasil dihapus');
        
    //     // $laporan = Laporan::where('id', $id)->delete();
    //     // return redirect('/dashboard')->with('sukses','Data berhasil dihapus');

    // }
    // public function show ($id)
    // {
    //     $laporan = Laporan::find($id);
    //     return view('subkoordinator.detaillaporan')->with(['laporan' => $laporan]);
    // }
}
