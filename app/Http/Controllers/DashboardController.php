<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Laporan;
use App\Tahapan;

use Auth;

class DashboardController extends Controller
{
    public function index() {
        
        $data['datauser'] = User::withTrashed()->where('role_id', '!=', 1)->get();
        $data['tahapans'] = Tahapan::all();
        $data['total'] = User::where('role_id','!=',1)->count();
        $data['subkoordinator'] = User::where('role_id','=',2)->count();
        $data['koordinator'] = User::where('role_id','=',3)->count();
        $data['direktur'] = User::where('role_id','=',4)->count();
        $data['laporan'] = Laporan::with('tahapan' )->get();
        // $data['userId'] = Auth::user()->id;
       
        if (Auth::user()->role_id == 1) {
            return view('admin.dashboard', $data);
        }
        elseif (Auth::user()->role_id !== 1) {
            return view('user.dashboard', $data);
        }

    }

   
}
