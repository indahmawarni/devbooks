<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Laporan;
use App\Requirement;
use App\Pengembangan;
use App\Testing;
use App\Rilis;
use App\Tahapan;
use Auth;
use PDF;

class RilisController extends Controller
{
    public function update(Request $request, $id)
    {
        $tgl_rilis = $request->tgl_rilis;
        $kesimpulan_rilis = $request->kesimpulan_rilis;
        Laporan::where('id', $id)->update(
            [
                'tgl_rilis' => $tgl_rilis,
                'kesimpulan_rilis' => $kesimpulan_rilis
            ]
        );
        return redirect()->route('detail-laporan', ['id' => $id])->with('success', 'Tanggal dan kesimpulan berhasil ditambahkan');
    }

    public function send(Request $r, $id){        

        if (Auth::user()->role_id == 2) {
            $laporan = Laporan::where('id', $id)->first();
            $tgl_rilis = $r->input('tgl_rilis');
            $kesimpulan_rilis = $r->input('kesimpulan_rilis');
            Laporan::where('id', $id)->update([
                'status' => 'Diajukan',
                'posisi' => 'Koordinator',
                'tgl_rilis' => $tgl_rilis,
                'kesimpulan_rilis' => $kesimpulan_rilis,
            ]);
            $rilis = Tahapan::where('laporan_id', $id)->get();
            foreach ($rilis as $rq) {
                if (isset($r->catatan_rilis[$rq->id])){
                    $catatan_rilis = $r->catatan_rilis[$rq->id];
                }
                else {
                    $catatan_rilis = '';
                }
                if (isset($r->status_rilis[$rq->id])){
                    $status_rilis = 1;
                }
                else {
                    $status_rilis = 0;
                }
                Tahapan::where('id', $rq->id)->update([
                    'catatan_rilis' => $catatan_rilis,
                    'status_rilis' => $status_rilis,
                   
                ]);
            } 
            return redirect()->route('detail-laporan',['id'=>$id])->with('success', 'Data berhasil dikirim');
        }
        elseif (Auth::user()->role_id == 3) {
            $laporan = Laporan::where('id', $id)->first();
            $tgl_rilis = $r->input('tgl_rilis');
            $kesimpulan_rilis = $r->input('kesimpulan_rilis');
            Laporan::where('id', $id)->update([
                'status' => 'Diajukan',
                'posisi' => 'Direktur',
                'tgl_rilis' => $tgl_rilis,
                'kesimpulan_rilis' => $kesimpulan_rilis,
            ]);
            $rilis = Tahapan::where('laporan_id', $id)->get();
            foreach ($rilis as $rq) {
                if (isset($r->catatan_rilis[$rq->id])){
                    $catatan_rilis = $r->catatan_rilis[$rq->id];
                }
                else {
                    $catatan_rilis = '';
                }
                if (isset($r->status_rilis[$rq->id])){
                    $status_rilis = 1;
                }
                else {
                    $status_rilis = 0;
                }
                Tahapan::where('id', $rq->id)->update([
                    'catatan_rilis' => $catatan_rilis,
                    'status_rilis' => $status_rilis,
                   
                ]);
            } 
            return redirect()->route('detail-laporan',['id'=>$id])->with('success', 'Data berhasil dikirim');
        }
        elseif (Auth::user()->role_id == 4) {
            $laporan = Laporan::where('id', $id)->first();
            $tgl_rilis = $r->input('tgl_rilis');
            $kesimpulan_rilis = $r->input('kesimpulan_rilis');
            Laporan::where('id', $id)->update([

                'status' => 'Disetujui',
                'posisi' => 'Direktur',
                'nama_tahapan' => 'selesai',
                'tgl_rilis' => $tgl_rilis,
                'kesimpulan_rilis' => $kesimpulan_rilis,
            ]);
            $rilis = Tahapan::where('laporan_id', $id)->get();
            foreach ($rilis as $rq) {
                if (isset($r->catatan_rilis[$rq->id])){
                    $catatan_rilis = $r->catatan_rilis[$rq->id];
                }
                else {
                    $catatan_rilis = '';
                }
                if (isset($r->status_rilis[$rq->id])){
                    $status_rilis = 1;
                }
                else {
                    $status_rilis = 0;
                }
                Tahapan::where('id', $rq->id)->update([
                    'catatan_rilis' => $catatan_rilis,
                    'status_rilis' => $status_rilis,
                    
                ]);
            } 
            return redirect()->route('detail-laporan',['id'=>$id])->with('success', 'Data berhasil disetujui');
        }      
    }

    public function generate($id)
    {
        $laporan = Laporan::findOrFail($id);
        $rilis = Rilis::all();

        $rilis = Tahapan::where('laporan_id', $id)->where('status_rilis', 1)->get();
        $pdf = PDF::loadview('user.generate_rilis', ['laporan' => $laporan, 'rilis' => $rilis]);
        return $pdf->stream();
    }
}
