<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Laporan;
use App\Tahapan;
use App\Fungsi;
use DB;
use Auth;
use App\User;
use App\Role;
use PDF;
use App\Requirement;
use App\Pengembangan;
use App\Testing;
use App\Rilis;


class LaporanController extends Controller
{
    // public function index(Request $request)
    // {   
    //     $tahapanId = $request->id_tahapan;
    //     return view('user.createlaporan',['tahapanId' => $tahapanId]);
    // }

    // public function index()
    // {   
    //     $fungsi = Fungsi::all();
    //     $tahapan = Tahapan::all();
    //     return view('user.fungsi',compact('fungsi','tahapan'));
    // }

    public function create(Request $request) 
    {
        $request->validate([
            'nama_aplikasi'         => 'required',
            'versi'                 => 'required',
            // 'jenis'                 => 'required',
        
        ], [
            'nama_aplikasi.required'  => 'Nama Aplikasi tidak boleh kosong',
            'versi.required'          => 'Versi tidak boleh kosong',
            // 'jenis.required'          => 'Jenis harus dipilih',
            
           
        ]);    
        
        $userId = Auth::id();
        $user = User::find($userId);
        // $tahapanId = Tahapan::where('id',1)->first();   
        $laporan = new Laporan;
        $laporan->nama_aplikasi = $request->nama_aplikasi;
        $laporan->versi = $request->versi;
        // $laporan->jenis = $request->jenis;
        $laporan->posisi = 'Subkoordinator' ;
        $laporan->status = 'Persiapan';
        // $laporan->kesimpulan = $request->kesimpulan;
        $laporan->subkoordinator_id = $userId;
        $laporan->nama_tahapan = 'User Requirement'; 
        $laporan->save();
        // dd($laporan);
       
        return redirect('/dashboard')->with('success','Data berhasil ditambahkan');
    }

    // public function edit($id) {
    //     $laporan = Laporan::findOrFail($id);
    //     // echo "<pre>";
    //     //         print_r($laporan->tahapan_id);
    //     // echo "</pre>";
    //     // exit;
    //     return view('user.editlaporan',['laporan' => $laporan]);
        
    // }

    public function update(Request $request, $id) 
    {
        $laporan = Laporan::findorFail($id);
        $laporan->update($request->all());
        return redirect('/dashboard')->with('success','Data berhasil diperbarui');
    }

    public function delete($id)
    {
        // print_r($id);exit;
            $laporan = Laporan::findOrFail($id);
            $tahapan = Tahapan::where('laporan_id', $id)->delete();
            // $laporan = Tahapan::where('laporan_id', $id);
            $laporan->delete();
            return redirect('/dashboard')->with('success','Data berhasil dihapus');
    }

    public function show (Request $request, $id)
    {
        $laporan = Laporan::find($id);
        $laporan = Laporan::where('id', $id)->first();
        // $requirement = Requirement::where('laporan_id', $id)->where('status_requirement', 1)->get();
        $requirement = Tahapan::where('laporan_id', $id)->get();
        // $tahapan = Tahapan::where('laporan_id', $id)->where('status_requirement', 1)->get();
        $pengembangan = Tahapan::where([
            'laporan_id' => $id,
            'status_requirement' => 1
        ])->get();
        $testing = Tahapan::where('laporan_id', $id)->where('status_requirement', 1)->get();
        
        $rilis = Tahapan::where('laporan_id', $id)->where('status_requirement', 1)->where('status_testing', '!=', null)->get();
        // $rilis = Rilis::where('laporan_id', $id)->get();
       
        return view('user.detaillaporan')->with(['laporan_id' => $id, 'requirement' => $requirement, 'pengembangan' => $pengembangan, 'testing' => $testing, 'rilis' => $rilis, 'laporan' => $laporan,]);

        // return view('user.detaillaporan', compact('laporan','fungsi'));
    }

    // public function generate($id)
    // {
        
    //     $laporan = Laporan::findOrFail($id);
    //     // $laporan = Laporan::where('direktur_id','!=','null')->get();
    //     $pdf = PDF::loadview('user.generatepdf',['laporan'=>$laporan]);
    //     return $pdf->stream();
    // }
    


}
