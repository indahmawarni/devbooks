<?php

namespace App\Http\Controllers\Direktur;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Laporan;
use App\Tahapan;
use DB;
use Auth;
use App\User;


class UpdateController extends Controller
{
    public function update(Request $request, $id) 
    {
        
        
        Laporan::find($id)->update(
            [
            $userId = Auth::id(),
            $user = User::find($userId),
            $laporan = Laporan::find($id),
            $laporan->tgl_laporan = $request->tgl_laporan,
            $laporan->versi = $request->versi,
            $laporan->judul = $request->judul,
            $laporan->perihal = $request->perihal,
            $laporan->jenis = $request->jenis,
            $laporan->isi = $request->isi,
            // $laporan->tahapan_id = $request->id_tahapan,
            $laporan->kesimpulan = $request->kesimpulan,
            $laporan->direktur_id = $userId,
            $laporan->posisi = 'Direktur' ,
            $laporan->status = 'Diajukan',
            $laporan->save(),
            ]
        );
        return redirect('/dashboard')->with('success','Data berhasil diperbarui');
    }

    public function kirim(Request $request, $id) 
    {
        Laporan::find($id)->update(
            [
            $laporan = Laporan::find($id),
            $userId = Auth::id(),
            $user = User::find($userId),
            $laporan->posisi = 'Direktur' ,
            $laporan->status = 'Disetujui',
            $laporan->direktur_id = $userId,
            $laporan->save(),
            ]
        );
        return redirect('/dashboard')->with('success','Data berhasil disetujui');
    }

    public function revisi(Request $request, $id) 
    {
        Laporan::find($id)->update(
            [
            $laporan = Laporan::find($id),
            $laporan->catatan_versi_koordinator = null,
            $laporan->catatan_versi_direktur = $request->catatan_versi_direktur,
            $laporan->posisi = 'Subkoordinator' ,
            $laporan->status = 'Revisi',
            $laporan->save(),
            ]
        );
        return redirect('/dashboard')->with('success','Data berhasil dikembalikan');
    }

}