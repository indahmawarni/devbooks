<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Laporan;
use App\Requirement;
use App\Pengembangan;
use App\Testing;
use App\Rilis;
use App\Tahapan;
use Auth;
use PDF;

class TestingController extends Controller
{
    // public function create(Request $request)
    // {
    //     $testing = new Testing;
    //     $rilis = new Rilis;
    //     $nama_fitur = $request->nama_fitur;
    //     $laporan_id = $request->laporan_id;
    //     foreach ($nama_fitur as $nf) {
    //         $data[] = ['nama_fitur' => $nf, 'laporan_id' => $laporan_id,];
    //     }
    //     $testing->insert($data);
    //     $rilis->insert($data);

    //     return redirect()->route('detail-laporan',['id'=>$laporan_id])->with('success', 'Data berhasil ditambahkan');
    // }

    // public function delete(Request $request, $id)
    // {
    //     $laporan_id = $request->laporan_id;
    //     $testing     =   Testing::findOrFail($id);
    //     $rilis       =   Rilis::findOrFail($id);          
    //     $testing->delete();
    //     $rilis->delete();        
    
    //     return redirect()->route('detail-laporan',['id'=>$laporan_id])->with('success', 'Data berhasil dihapus');        
    // }

    // public function updatefitur(Request $request, $id)
    // {
    //     $nama_fitur = $request->nama_fitur;
    //     $laporan_id = $request->laporan_id;
    //     $testing     = Testing::findOrFail($id);
    //     $rilis       =  Rilis::findOrFail($id);
    //     $testing->nama_fitur = $nama_fitur;
    //     $rilis->nama_fitur = $nama_fitur;
    //     $testing->save();
    //     $rilis->save();
    //     return redirect()->route('detail-laporan',['id'=>$laporan_id])->with('success', 'Data berhasil diperbarui');
    // }

    public function update(Request $request, $id)
    {
        $tgl_testing = $request->tgl_testing;
        $kesimpulan_testing = $request->kesimpulan_testing;
        Laporan::where('id', $id)->update(
            [
                'tgl_testing' => $tgl_testing,
                'kesimpulan_testing' => $kesimpulan_testing,
            ]
        );
        return redirect()->route('detail-laporan', ['id' => $id])->with('success', 'Tanggal dan kesimpulan berhasil ditambahkan');
    }

    public function send(Request $r, $id){        

        if (Auth::user()->role_id == 2) {
            $laporan = Laporan::where('id', $id)->first();
            $tgl_testing = $r->input('tgl_testing');
            $kesimpulan_testing = $r->input('kesimpulan_testing');
            Laporan::where('id', $id)->update([
                'status' => 'Diajukan',
                'posisi' => 'Koordinator',
                'tgl_testing' => $tgl_testing,
                'kesimpulan_testing' => $kesimpulan_testing,
            ]);
            $testing = Tahapan::where('laporan_id', $id)->get();
            foreach ($testing as $rq) {
                if (isset($r->catatan_testing[$rq->id])){
                    $catatan_testing = $r->catatan_testing[$rq->id];
                }
                else {
                    $catatan_testing = '';
                }
                if (isset($r->status_testing[$rq->id])){
                    $status_testing = 1;
                }
                else {
                    $status_testing = 0;
                }
                Tahapan::where('id', $rq->id)->update([
                    'catatan_testing' => $catatan_testing,
                    'status_testing' => $status_testing,
                    'status_rilis' =>$status_testing,
                ]);
            } 
            return redirect()->route('detail-laporan',['id'=>$id])->with('success', 'Data berhasil dikirim');
        }
        elseif (Auth::user()->role_id == 3) {
            $laporan = Laporan::where('id', $id)->first();
            $tgl_testing = $r->input('tgl_testing');
            $kesimpulan_testing = $r->input('kesimpulan_testing');
            Laporan::where('id', $id)->update([
                'status' => 'Diajukan',
                'posisi' => 'Direktur',
                'tgl_testing' => $tgl_testing,
                'kesimpulan_testing' => $kesimpulan_testing,
            ]);
            $testing = Tahapan::where('laporan_id', $id)->get();
            foreach ($testing as $rq) {
                if (isset($r->catatan_testing[$rq->id])){
                    $catatan_testing = $r->catatan_testing[$rq->id];
                }
                else {
                    $catatan_testing = '';
                }
                if (isset($r->status_testing[$rq->id])){
                    $status_testing = 1;
                }
                else {
                    $status_testing = 0;
                }
                Tahapan::where('id', $rq->id)->update([
                    'catatan_testing' => $catatan_testing,
                    'status_testing' => $status_testing,
                    'status_rilis' =>$status_testing,
                ]);
            } 
            return redirect()->route('detail-laporan',['id'=>$id])->with('success', 'Data berhasil dikirim');
        }
        elseif (Auth::user()->role_id == 4) {
            $laporan = Laporan::where('id', $id)->first();
            $tgl_testing = $r->input('tgl_testing');
            $kesimpulan_testing = $r->input('kesimpulan_testing');
            Laporan::where('id', $id)->update([

                'status' => 'Persiapan',
                'posisi' => 'Subkoordinator',
                'nama_tahapan' => 'Rilis',
                'tgl_testing' => $tgl_testing,
                'kesimpulan_testing' => $kesimpulan_testing,
            ]);
            $testing = Tahapan::where('laporan_id', $id)->get();
            foreach ($testing as $rq) {
                if (isset($r->catatan_testing[$rq->id])){
                    $catatan_testing = $r->catatan_testing[$rq->id];
                }
                else {
                    $catatan_testing = '';
                }
                if (isset($r->status_testing[$rq->id])){
                    $status_testing = 1;
                }
                else {
                    $status_testing = 0;
                }
                Tahapan::where('id', $rq->id)->update([
                    'catatan_testing' => $catatan_testing,
                    'status_testing' => $status_testing,
                    'status_rilis' =>$status_testing,
                ]);
            } 
            return redirect()->route('detail-laporan',['id'=>$id])->with('success', 'Data berhasil disetujui');
        }      
    }

    public function generate($id)
    {
        $laporan = Laporan::findOrFail($id);
        $testing = Tahapan::where('laporan_id', $id)->where('status_testing', 1)->get();
        $pdf = PDF::loadview('user.generate_testing', ['laporan' => $laporan, 'testing' => $testing]);
        return $pdf->stream();
    }
}
