<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Crypt;
use App\User;
use App\Sandi;

class SandiController extends Controller
{
    public function store(Request $request) {
        $sandi = \App\Sandi::create($request->password());
        $user = new \App\User;
        $user->nama = $request->nama;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Crypt::encryptString($request->password);
        $user->role = $request->role;
        $user->remember_token = str_random(60);
        $user->save();

        $sandi = new \App\Sandi;
        $sandi->password = $request->password;
        $sandi->save();

        return redirect('/dashboard')->with('sukses','Data berhasil ditambahkan');
    }
}

// $user              = new User();
// $sandi->password   = Crypt::encryptString($request->password);
// $sandi->save();
// return view('admin.dashboard')
// $sandi            = new Sandi();
// $sandi->password   = Crypt::encryptString($request->password);
// $sandi->save();
