<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Laporan;
use App\Requirement;
use App\Pengembangan;
use App\Testing;
use App\Rilis;
use App\Tahapan;
use Auth;
use PDF;
use User;

class RequirementController extends Controller
{

    public function update(Request $request, $id)
    {
        $tgl_requirement = $request->tgl_requirement;
        $kesimpulan_requirement = $request->kesimpulan_requirement;

        Laporan::where('id', $id)->update(
            [
                'tgl_requirement' => $tgl_requirement,
                'kesimpulan_requirement' => $kesimpulan_requirement
            ]
        );
        return redirect()->route('detail-laporan', ['id' => $id])->with('success', 'Tanggal dan kesimpulan berhasil ditambahkan');
    }

    public function send(Request $r, $id){
       
        if (Auth::user()->role_id == 2) {
            $laporan = Laporan::where('id', $id)->first();
           $tgl_requirement = $r->input('tgl_requirement');
           $kesimpulan_requirement = $r->input('kesimpulan_requirement');
            // if (isset($r->kesimpulan_requirement[$laporan->id])){
            //     $kesimpulan_requirement = $r->kesimpulan_requirement[$laporan->id];
            // }
            // else {
            //     $kesimpulan_requirement = '';
            // }
            Laporan::where('id', $id)->update([
               
                'status' => 'Diajukan',
                'posisi' => 'Koordinator',
                'tgl_requirement' => $tgl_requirement,
                'kesimpulan_requirement' => $kesimpulan_requirement,
                
  
            ]);
            $requirement = Tahapan::where('laporan_id', $id)->get();
            foreach ($requirement as $rq) {
                if (isset($r->catatan_requirement[$rq->id])){
                    $catatan_requirement = $r->catatan_requirement[$rq->id];
                }
                else {
                    $catatan_requirement = '';
                }
                if (isset($r->status_requirement[$rq->id])){
                    $status_requirement = 1;
                }
                else {
                    $status_requirement = 0;
                }
                Tahapan::where('id', $rq->id)->update([
                    'catatan_requirement' => $catatan_requirement,
                    'status_requirement' => $status_requirement,
                    'status_pengembangan' => $status_requirement,
                ]);
            } 
            
            return redirect()->route('detail-laporan',['id'=>$id])->with('success', 'Data berhasil dikirim');
        }
        elseif (Auth::user()->role_id == 3) {
            $laporan = Laporan::where('id', $id)->first();
           $tgl_requirement = $r->input('tgl_requirement');
           $kesimpulan_requirement = $r->input('kesimpulan_requirement');
            Laporan::where('id', $id)->update([
                'status' => 'Diajukan',
                'posisi' => 'Direktur',
                'tgl_requirement' => $r->tgl_requirement,
                'kesimpulan_requirement' => $kesimpulan_requirement,
            ]);
            $requirement = Tahapan::where('laporan_id', $id)->get();
            foreach ($requirement as $rq) {
                if (isset($r->catatan_requirement[$rq->id])){
                    $catatan_requirement = $r->catatan_requirement[$rq->id];
                }
                else {
                    $catatan_requirement = '';
                }
                if (isset($r->status_requirement[$rq->id])){
                    $status_requirement = 1;
                }
                else {
                    $status_requirement = 0;
                }
                Tahapan::where('id', $rq->id)->update([
                    'catatan_requirement' => $catatan_requirement,
                    'status_requirement' => $status_requirement,
                    'status_pengembangan' => $status_requirement,

                ]);
            } 
            return redirect()->route('detail-laporan',['id'=>$id])->with('success', 'Data berhasil dikirim');
        }
        elseif (Auth::user()->role_id == 4) {
            $laporan = Laporan::where('id', $id)->first();
           $tgl_requirement = $r->input('tgl_requirement');
           $kesimpulan_requirement = $r->input('kesimpulan_requirement');
            Laporan::where('id', $id)->update([

                'status' => 'Persiapan',
                'posisi' => 'Subkoordinator',
                'nama_tahapan' => 'Pengembangan',
                'tgl_requirement' => $r->tgl_requirement,
                'kesimpulan_requirement' => $kesimpulan_requirement,
            ]);
            $requirement = Tahapan::where('laporan_id', $id)->get();
            foreach ($requirement as $rq) {
                if (isset($r->catatan_requirement[$rq->id])){
                    $catatan_requirement = $r->catatan_requirement[$rq->id];
                }
                else {
                    $catatan_requirement = '';
                }
                if (isset($r->status_requirement[$rq->id])){
                    $status_requirement = 1;
                }
                else {
                    $status_requirement = 0;
                }
                Tahapan::where('id', $rq->id)->update([
                    'catatan_requirement' => $catatan_requirement,
                    'status_requirement' => $status_requirement,
                    'status_pengembangan' => $status_requirement,
                ]);
            } 
            return redirect()->route('detail-laporan',['id'=>$id])->with('success', 'Data berhasil disetujui');
        }      
    }

    public function generate($id)
    {
        $laporan = Laporan::findOrFail($id);
        $requirement = Tahapan::where('laporan_id', $id)->where('status_requirement', 1)->get();

        // $laporan = Laporan::where('direktur_id','!=','null')->get();
        $pdf = PDF::loadview('user.generate_requirement', ['laporan' => $laporan, 'requirement' => $requirement]);
        return $pdf->stream();
    }
}
