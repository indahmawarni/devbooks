<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rilis extends Model
{
    public $fillable = [
        'nama_fitur',
    ];
    public function laporan() {
        return $this->belongsTo(Laporan::class);
    }
}
