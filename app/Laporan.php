<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Laporan extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'nama_aplikasi', 'versi', 'jenis', 'tgl_requirement', 'tgl_pengembangan', 'tgl_testing', 'tgl_rilis', 'isi_requirement', 'isi_pengembangan', 'isi_testing', 'isi_rilis', 'kesimpulan_requirement', 'kesimpulan_pengembangan', 'kesimpulan_testing', 'kesimpulan_rilis', 'catatan_versi_koordinator', 'catatan_versi_direktur', 'nama_tahapan',
    ];
    
    public function user() {
        return $this->belongsTo(User::class);
    }
    public function tahapan() {
        return $this->belongsTo(Tahapan::class);
        
    }
    public function role() {
        return $this->belongsTo(Role::class);
        
    }
    public function direktur_id() {
        return $this->belongsTo(User::class);
    }
   
    public function posisi() {
        return $this->hasOne(Role::class, 'id', 'posisi');
    }
    public function subkoordinator() {
        return $this->hasOne(User::class, 'id', 'subkoordinator_id');
    }
    public function koordinator() {
        return $this->hasOne(User::class, 'id', 'koordinator_id');
    }
    public function direktur() {
        return $this->hasOne(User::class, 'id', 'direktur_id');
    }
    // public function getCreatedAtAttribute() 
    // {
    //     return Carbon::parse($this->attributes['tgl_laporan'])
    //     ->translatedFormat('d-m-Y');
    // }

}
