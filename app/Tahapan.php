<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tahapan extends Model
{
    // protected $fillable = [
    //     'nama',
    // ];
    // public function laporan() {
    //     return $this->hasOne(Laporan::class);
    // }

    public $fillable = [
        'nama_fitur', 'status_requirement', 'status_pengembangan', 'status_testing', 'status_rilis', 'catatan_requirement', 'catatan_pengembangan', 'catatan_testing', 'catatan_rilis'
    ];
    public function laporan() {
        return $this->belongsTo(Laporan::class);
    }
    public function user() {
        return $this->belongsTo(User::class, 'id', 'role_id');
    }
    public function subkoordinator() {
        return $this->hasOne(User::class, 'id', 'subkoordinator_id');
    }
    public function koordinator() {
        return $this->hasOne(User::class, 'id', 'koordinator_id');
    }
    public function direktur() {
        return $this->hasOne(User::class, 'id', 'direktur_id');
    }
}
