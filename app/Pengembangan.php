<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengembangan extends Model
{
    public $fillable = [
        'nama_fitur',
    ];
    public function laporan() {
        return $this->belongsTo(Laporan::class);
    }
}
