<?php

use Illuminate\Database\Seeder;
use App\Tahapan;

class TahapansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tahapan::truncate();
        Tahapan::create(["nama"=>"User Requirement"]);
        Tahapan::create(["nama"=>"Pengembangan"]);
        Tahapan::create(["nama"=>"Testing"]);
        Tahapan::create(["nama"=>"Rilis"]);

    }
}
