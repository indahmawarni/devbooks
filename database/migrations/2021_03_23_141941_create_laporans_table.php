<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;




class CreateLaporansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tgl_laporan');
            $table->string('judul',100);
            $table->text('perihal',100);
            $table->text('isi');
            $table->text('kesimpulan');
            $table->string('versi',10);
            $table->bigInteger('tahapan_id')->unsigned();
            $table->foreign('tahapan_id')->references('id')->on('tahapans');
            $table->string('jenis',20);
            $table->smallinteger('status')->nullable();
            $table->string('posisi',20)->nullable();
            $table->text('catatan_versi_koordinator')->nullable();
            $table->text('catatan_versi_direktur')->nullable();
            $table->integer('subkoordinator_id')->nullable();
            $table->integer('koordinator_id')->nullable();
            $table->integer('direktur_id')->nullable();          
            $table->string('generate',100)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporans');
    }
}