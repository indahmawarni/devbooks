@extends('layouts.master')

@section('title', 'Dashboard')
@section('topnav')
    @include('layouts.partials.topnav')
@endsection

@section('sidenav')
    @include('layouts.partials.sidenav')
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">User Requirement</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Detail</li>
        </ol>
        
        <div class="card my-4">
            <div class="card-header">
                Detail Laporan
            </div>
            <br><br>
            <div class="container">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                          <tr >
                            <th scope="col" class="detail">Tanggal</th>
                            <td> {{ $laporan->tgl_laporan }} </td>
                          </tr>
                          <tr>
                            <th scope="col" class="detail">Judul</th>
                            <td>{{ $laporan->judul }}</td>
                          </tr>
                          <tr>
                            <th scope="col" class="detail">Jenis</th>
                            <td>{{ $laporan->jenis }}</td>
                          </tr>
                          <tr>
                            <th scope="col" class="detail">Isi</th>
                            <td>{!! $laporan->isi !!}</td>
                          </tr>
                          <tr>
                            <th scope="col" class="detail">Kesimpulan/Saran</th>
                            <td>{{ $laporan->kesimpulan }}</td>
                          </tr>
                        </tbody>
                    </table>
                    <div class="form-group mt-5 catatan">
                        <label for="catatan" class="col-form-label"><b>Catatan ke Subkoordinator</b></label>
                        <div class="pl-0 col-md-12 mb-3 ">
                            <textarea name="catatan" id="catatan" class="form-control" ></textarea>
                        </div>
                        <button type="submit" class="btn btn-info ml-0">Kembalikan ke Subkoordinator</button>  
                    </div> 
                </div>
            </div>
        </div>
        </div>
    </div>
</main>
@endsection

@section('footer')
    @include('layouts.partials.footer')
@endsection