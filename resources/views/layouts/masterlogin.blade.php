<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Login</title>
        <link rel="shortcut icon" href="{{ asset('image/favicon-32x32.png')}}">
        <script src="https://kit.fontawesome.com/48a5c3dd7e.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.7.0/tinymce.min.js"></script>

        <!-- CSS -->
        <link href="{{ asset('css/style.css')}}" rel="stylesheet" />
        <link href="{{ asset('css/custom.css')}}" rel="stylesheet" />
        

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Lato&family=Montserrat&family=Poppins&display=swap"    rel="stylesheet">

        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
        <!-- CSS -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="{{ asset ('css/style.css') }}">

      <!-- Font Awesome-->
      <script src="https://kit.fontawesome.com/48a5c3dd7e.js" crossorigin="anonymous"></script>
        
    </head>
    <body>
        <div class="body-login">
        @yield('content')
        </div>
    </body>


<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</html>
