
<nav class="sb-sidenav accordion sidenav-custom" id="sidenavAccordion">
    <div class="sb-sidenav-menu">
        <div class="nav">
            <div class="sb-sidenav-menu-heading"></div>
            <hr class="sidebar-divider my-0">
            <a class="nav-link" href="{{ url('/dashboard') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                Dashboard
            </a>
            <hr class="sidebar-divider my-0">
            @if (Auth::user()->role_id !== 1)
            <a class="nav-link" href="{{ url('profil/'.Auth::user()->id) }}">
                <div class="sb-nav-link-icon"><i class="far fa-user"></i></div>
                Profil
            </a>
            @endif
            @if (Auth::user()->role_id == 1)
                <div class="sb-sidenav-menu-heading">Master Data</div>
                
                <a class="nav-link collapsed" href="{{ url('/unitkerja') }}" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                    Unit Kerja
                </a>
                
            
                {{-- <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Jenis Perbaikan
                </a> --}}
            @endif
                <hr class="sidebar-divider my-0">
                {{-- <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="layout-static.html">Static Navigation</a>
                        <a class="nav-link" href="layout-sidenav-light.html">Light Sidenav</a>
                    </nav>
                </div> --}}
                <a class="nav-link" href="/logout">
                    <div class="sb-nav-link-icon"><i class="fas fa-sign-out-alt"></i></div>
                    Keluar
                </a>
           
            {{-- <div class="nav-link logout">
                <form action="{{route("logout")}}" method="POST">
                    @csrf
                    <button class="sb-nav-link-icon"><i class="fas fa-sign-out-alt">
                       </i>Keluar</button>
                </form>
            </div>
             --}}
             {{-- <div class="nav-link logout">
                <form action="{{route("logout")}}" method="POST">
                    @csrf
                    
                    <button class="sb-nav-link-icon"><i class="fas fa-sign-out-alt"><div class="keluar"> Keluar</div>
                       </i></button>
                </form>
            </div> --}}
            

             
                    {{-- <button class="dropdown-item"><i class="fas fa-sign-out-alt"></i> Logout</button> --}}
            
        </div>
    </div>
</nav>
