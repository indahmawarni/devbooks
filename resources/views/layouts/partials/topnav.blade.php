<nav class="sb-topnav navbar navbar-expand ">
        <a class="navbar-brand" href="#"><img src="{{ asset('image/devbooks.png') }}" alt=""></a>
        <button class="btn btn-link btn-sm order-1 order-lg-0 text-white" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>

        <!-- Navbar-->
        <ul class="navbar-nav ml-auto mr-md-0 ">
           
                <span class="text-white" role="button" aria-haspopup="true" aria-expanded="false"><span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user()->nama}}</span><i class="fas fa-user fa-fw"></i></span>
                {{-- <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <form action="{{route("logout")}}" method="POST">
                        @csrf
                        <button class="dropdown-item"><i class="fas fa-sign-out-alt"></i> Logout</button>
                    </form>
                </div> --}}
                

        </ul>
</nav>