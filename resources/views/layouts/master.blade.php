<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>@yield('title','Devbooks')</title>
        <link rel="shortcut icon" href="{{ asset('image/favicon-32x32.png')}}">
        
        <!-- Tinymce -->
        {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.7.0/tinymce.min.js"></script> --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.0/tinymce.min.js"></script>
        <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
        


        <!-- CSS -->
        <link href="{{ asset('css/style.css')}}" rel="stylesheet" />
        <link href="{{ asset('css/custom.css')}}" rel="stylesheet" />
        
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Lato&family=Montserrat&family=Poppins&display=swap" rel="stylesheet">

        <!-- DataTables -->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
        
        <!-- Font Awesome -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script> -->
        <script src="https://kit.fontawesome.com/48a5c3dd7e.js" crossorigin="anonymous"></script>

        {{-- Step Wizard --}}
      

       {{-- @yield('head') --}}

      
       
        
    </head>
    <body class="sb-nav-fixed">
        @yield('topnav')
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                @yield('sidenav')
            </div>
            <div id="layoutSidenav_content">
                @yield('content')
                <footer class="py-4 bg-light mt-auto">
                    @yield('footer')
                </footer>
            </div>
        </div>
        
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('js/script.js')}}"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('tampilan/dist/assets/demo/datatables-demo.js')}}"></script>
       
        @yield('js')
    </body>
</html>
