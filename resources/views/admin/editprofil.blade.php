@extends('layouts.master')

@section('title', 'Ubah Profil')
@section('topnav')
    @include('layouts.partials.topnav')
@endsection

@section('sidenav')
    @include('layouts.partials.sidenav')
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Ubah Profil</li>
        </ol>
        <!-- Isi Form -->  
            <div class="row justify-content-center ">
                <div class="card mt-4 mb-5 col-md-6 px-0">
                    <div class="card-header">
                        <i class="fas fa-user-edit"></i>
                        Ubah Profil
                    </div>
                    <div class="card-body form-admin">
                        <form action="{{url('profil/admin/update/'.$user->id)}}" method="POST">
                            @method('PUT')
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="required" for="exampleInputEmail1">Email</label>
                                <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ old('email', $user->email) }}" autofocus>
                                @error('email')
                                    <div class="invalid-feedback">
                                        {{ $message}}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="required" for="exampleInputPassword1">Password</label>
                                <input name="password" type="password" class="form-control mb-1 @error('password') is-invalid @enderror" id="myInput" value="{{ old('password', $user->sandi['password']) }}" autofocus >
                                <input type="checkbox" onclick="myFunction()"> Show Password
                                @error('password')
                                    <div class="invalid-feedback">
                                        {{ $message}}
                                    </div>
                                @enderror
                                
                            </div>
                            <div class="form-group">
                                <label class="required" for="inputUsername">Username</label>
                                <input name="username" type="text" class="form-control @error('username') is-invalid @enderror" id="inputUsername" value="{{ old('username', $user->username) }}" autofocus>
                                @error('username')
                                    <div class="invalid-feedback">
                                        {{ $message}}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="required" for="inputNama">Nama Lengkap</label>
                                <input name="nama" type="text" class="form-control @error('nama') is-invalid @enderror" id="inputNama" value="{{ old('nama', $user->nama) }}" autofocus>
                                @error('nama')
                                    <div class="invalid-feedback">
                                        {{ $message}}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="required" for="exampleFormControlSelect1">Role</label>
                                <select name="role" class="form-control @error('role') is-invalid @enderror" id="exampleFormControlSelect1" autofocus>
                                    <option disabled>- Pilih -</option>
                                    @foreach ($roles as $role)
                                         <option value="{{ ($role->id)}}"
                                            @if ($role->id === $user->role_id) selected @endif>{{$role->name}}</option>
                                    @endforeach
                                    @error('role')
                                    <div class="invalid-feedback">
                                        {{ $message}}
                                    </div>
                                @enderror
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="required" for="exampleFormControlSelect1">Unit Kerja</label>
                                <select name="unitkerja" class="form-control @error('unitkerja') is-invalid @enderror" id="exampleFormControlSelect1" autofocus>
                                    <option disabled>- Pilih -</option>
                                    @foreach ($unitkerja as $item)
                                         <option value="{{ ($item->id)}}"
                                            @if ($item->id === $user->unitkerja_id) selected @endif>{{$item->nama}}</option>
                                    @endforeach
                                    @error('unitkerja')
                                    <div class="invalid-feedback">
                                        {{ $message}}
                                    </div>
                                    @enderror
                                </select>
                            </div>

                            {{-- <div class="form-group">
                                <label for="exampleFormControlSelect1">Direktorat</label>
                                <select name="direktorat" class="form-control" id="exampleFormControlSelect1" required>
                                    
                                    <option value="Biro Perencanaan dan Keuangan" @if ($user->direktorat == 'Biro Perencanaan dan Keuangan') selected @endif>Biro Perencanaan dan Keuangan</option>
                                    <option value="Biro Hukum, Organisasi, dan SDM" @if ($user->direktorat == 'Biro Hukum, Organisasi, dan SDM') selected @endif>Biro Hukum, Organisasi, dan SDM</option>
                                    <option value="Biro Humas, Sistem Informasi dan Umum" @if ($user->direktorat == 'Biro Humas, Sistem Informasi dan Umum') selected @endif>Biro Humas, Sistem Informasi dan Umum</option>
                                    <option value="Direktorat Pengembangan Profesi dan Kelembagaan" @if ($user->direktorat == 'Direktorat Pengembangan Profesi dan Kelembagaan') selected @endif>Direktorat Pengembangan Profesi dan Kelembagaan</option>
                                    <option value="Direktorat Sertifikasi Profesi" @if ($user->direktorat == 'Direktorat Sertifikasi Profesi') selected @endif>Direktorat Sertifikasi Profesi</option>
                                    <option value="Direktorat Advokasi Pemerintah Pusat" @if ($user->direktorat == 'Direktorat Advokasi Pemerintah Pusat') selected @endif>Direktorat Advokasi Pemerintah Pusat</option>
                                    <option value="Direktorat Advokasi Pemerintah Daerah" @if ($user->direktorat == 'Direktorat Advokasi Pemerintah Daerah') selected @endif>Direktorat Advokasi Pemerintah Daerah</option>
                                    <option value="Direktorat Penanganan Permasalahan Hukum" @if ($user->direktorat == 'Direktorat Penanganan Permasalahan Hukum') selected @endif>Direktorat Penanganan Permasalahan Hukum</option>
                                    <option value="Direktorat Pengembangan Sistem Katalog" @if ($user->direktorat == 'Direktorat Pengembangan Sistem Katalog') selected @endif>Direktorat Pengembangan Sistem Katalog</option>
                                    <option value="Direktorat Perencanaan, Monitoring dan Evaluasi Pengadaan" @if ($user->direktorat == 'Direktorat Perencanaan, Monitoring dan Evaluasi Pengadaan') selected @endif>Direktorat Perencanaan, Monitoring dan Evaluasi Pengadaan</option>
                        
                                </select>
                            </div> --}}
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>

                    </div>
                </div>
            </div>
    </div>
</main>
@endsection

@section('footer')
    @include('layouts.partials.footer')
@endsection
<script>
    function myFunction() {
      var x = document.getElementById("myInput");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
</script>