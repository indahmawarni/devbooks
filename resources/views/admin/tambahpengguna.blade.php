@extends('layouts.master')

@section('title', 'Devbooks')
@section('topnav')
    @include('layouts.partials.topnav')
@endsection

@section('sidenav')
    @include('layouts.partials.sidenav')
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Tambah Data</li>
        </ol>
        <!-- Isi Form -->  
            <div class="row justify-content-center ">
                <div class="card mt-4 mb-5 col-md-6 px-0">
                    <div class="card-header">
                        <i class="far fa-plus-square mr-1"></i>
                        Tambah Data
                    </div>
                    <div class="card-body form-admin">
                        <form action="{{url('tambah-pengguna')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="exampleInputEmail1" class = "required">Email</label>
                                <input name="email" class="form-control @error('email') is-invalid @enderror" id="exampleInputEmail1"  aria-describedby="emailHelp" value="{{ old('email') }}" autofocus >
                                @error('email')
                                    <div class="invalid-feedback">
                                        {{ $message}}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="inputPassword" class = "required">Password</label>
                                <input name="password" value="{{ old('password') }}" id= "myInput" type="password" class="form-control mb-1 @error('password') is-invalid @enderror" autofocus >
                                <input type="checkbox" onclick="myFunction()"> Show Password
                                @error('password')
                                    <div class="invalid-feedback">
                                        {{ $message}}
                                    </div>
                                @enderror
                            </div>
                           

                            <div class="form-group">
                                <label for="inputUsername" class = "required">Username</label>
                                <input name="username" value="{{ old('username') }}" type="username" class="form-control @error('username') is-invalid @enderror" id="inputUsername" autofocus>
                                @error('username')
                                    <div class="invalid-feedback">
                                        {{ $message}}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="inputNama" class = "required">Nama Lengkap</label>
                                <input name="nama" value="{{ old('nama') }}" type="text" class="form-control @error('nama') is-invalid @enderror" id="inputNama" autofocus>
                                @error('nama')
                                    <div class="invalid-feedback">
                                        {{ $message}}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1" class = "required">Role</label>
                                <select name="role" class="form-control @error('role') is-invalid @enderror" id="exampleFormControlSelect1" autofocus>
                                    <option value="{{ old('role') }}">- Pilih -</option>
                                    @foreach ($roles as $role)
                                         <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                                @error('role')
                                <div class="invalid-feedback">
                                    {{ $message}}
                                </div>
                                @enderror
                            </div>

                            {{-- <div class="form-group">
                                <label for="exampleFormControlSelect1" >Role</label>
                                <select name="role" class="form-control @error('role') is-invalid @enderror" id="exampleFormControlSelect1" autofocus>
                                    <option value="">- Pilih -</option>
                                    @foreach ($roles as $role)
                                         <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach

                                </select>
                                @error('role')
                                    <div class="invalid-feedback">
                                        {{ $message}}
                                    </div>
                                @enderror
                            </div> --}}

                            <div class="form-group">
                                <label for="exampleFormControlSelect1" class = "required">Unit Kerja</label>
                                <select name="unitkerja" class="form-control @error('unitkerja') is-invalid @enderror" id="exampleFormControlSelect1" autofocus>
                                    <option value="">- Pilih -</option>
                                    @foreach ($unitkerja as $item)
                                         <option value="{{$item->id}}">{{$item->nama}}</option>
                                    @endforeach
                                </select>
                                @error('unitkerja')
                                    <div class="invalid-feedback">
                                        {{ $message}}
                                    </div>
                                @enderror
                            </div>
                            {{-- <div class="form-group">
                                <label for="exampleFormControlSelect1" >Direktorat</label>
                                <select name="direktorat" class="form-control" id="exampleFormControlSelect1" required>
                                    <option value="">- Pilih -</option>
                                    <option value="Biro Perencanaan dan Keuangan">Biro Perencanaan dan Keuangan</option>
                                    <option value="Biro Hukum, Organisasi, dan SDM">Biro Hukum, Organisasi, dan SDM</option>
                                    <option value="Biro Humas, Sistem Informasi dan Umum">Biro Humas, Sistem Informasi dan Umum</option>
                                    <option value="Direktorat Pengembangan Profesi dan Kelembagaan">Direktorat Pengembangan Profesi dan Kelembagaan</option>
                                    <option value="Direktorat Sertifikasi Profesi">Direktorat Sertifikasi Profesi</option>
                                    <option value="Direktorat Advokasi Pemerintah Pusat">Direktorat Advokasi Pemerintah Pusat</option>
                                    <option value="Direktorat Advokasi Pemerintah Daerah">Direktorat Advokasi Pemerintah Daerah</option>
                                    <option value="Direktorat Penanganan Permasalahan Hukum">Direktorat Penanganan Permasalahan Hukum</option>
                                    <option value="Direktorat Pengembangan Sistem Katalog">Direktorat Pengembangan Sistem Katalog</option>
                                    <option value="Direktorat Perencanaan, Monitoring dan Evaluasi Pengadaan">Direktorat Perencanaan, Monitoring dan Evaluasi Pengadaan</option>
                                    
                                </select>
                            </div> --}}
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </form>

                    </div>
                </div>
            </div>
    </div>
</main>
@endsection

@section('footer')
    @include('layouts.partials.footer')
@endsection
<script>
    function myFunction() {
      var x = document.getElementById("myInput");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
</script>