@extends('layouts.master')

@section('title', 'Devbooks')
@section('topnav')
    @include('layouts.partials.topnav')
@endsection

@section('sidenav')
    @include('layouts.partials.sidenav')
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-2">
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        
        <!-- Content Row -->

        <!-- Button trigger modal -->
            <button type="button" class="btn btn-info mt-3" data-toggle="modal" data-target="#ModalTambah"><i class="fas fa-plus-circle mr-2"></i>
                Tambah
            </button>
        <!-- End Button trigger modal -->
            
            <!-- Modal Tambah Data-->
            <div class="modal fade" id="ModalTambah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{url('tambah-unitkerja')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="inputUnitKerja">Nama Unit Kerja</label>
                                <input name="nama" type="text" class="form-control" id="inputDirektorat" required>
                            </div>
                        
                    </div>
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        </form>
                   
                    </div>
                </div>
                </div>
            </div>
            <!-- End Modal -->
        
        {{-- Start Card --}}
        <div class="card my-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                Daftar Unit Kerja
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($unitkerja as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->nama }}</td>
                            <td>
                                <!-- Button Ubah -->
                                <div class="btn-group">
                                    <a href="" type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modalUbah-{{ $item->id }}"><i class="fas fa-edit"></i>     
                                    </a>
                                </div>
                                {{-- Button Hapus --}}
                                <div class="btn-group">
                                    <a href="" type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modalHapus-{{ $item->id }}"><i class="far fa-trash-alt"></i>
                                    </a>  
                                </div>
                            </td>
                    </tr>
                        @endforeach
                    </tbody>    
                </table>
            </div>
        </div>
        {{-- End Card --}}
    </div>
</main>

    <!-- Modal Ubah Data-->
@foreach ($unitkerja as $data )
    <div class="modal fade" id="modalUbah-{{ $data->id }}" tabindex="-1" aria-labelledby="modalUbah" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ubah Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form class="ubah" action="{{url('/update-unitkerja/'.$data->id)}}" method="POST">
                    @method('PUT')
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="inputUnitKerja">Nama Unit Kerja</label>
                        <input name="nama" type="text" class="form-control" id="formNama" value="{{ $data->nama}}" required>
                    </div>  
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </form>
            </div>
        </div>
        </div>
    </div>
    



    <!-- Modal Hapus-->
    <div class="modal fade" id="modalHapus-{{ $data->id }}" tabindex="-1" aria-labelledby="modalHapus" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="modalHapus">Hapus Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                Apakah Anda yakin ingin menghapus?
                </div>
                <div class="modal-footer">
                    <form class="delete" action="{{url('/hapus-unitkerja/'.$data->id)}}"  method="POST">
                    @method('delete')
                    @csrf      
                    <button class="btn btn-danger">Hapus
                    </button>
                    </form>  
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
@endforeach



@include('sweetalert::alert')
@endsection

@section('footer')
    @include('layouts.partials.footer')
@endsection
