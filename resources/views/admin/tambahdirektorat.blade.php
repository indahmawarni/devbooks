@extends('layouts.master')

@section('title', 'Devbooks')
@section('topnav')
    @include('layouts.partials.topnav')
@endsection

@section('sidenav')
    @include('layouts.partials.sidenav')
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Tambah Data</li>
        </ol>
        <!-- Isi Form -->  
            <div class="row justify-content-center ">
                <div class="card mt-4 mb-5 col-md-6 px-0">
                    <div class="card-header">
                        <i class="far fa-plus-square mr-1"></i>
                        Tambah Data
                    </div>
                    <div class="card-body form-admin">
                        <form action="{{url('tambah-direktorat')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="inputUsername">Nama Direktorat</label>
                                <input name="direktorat" type="text" class="form-control" id="inputDirektorat" required>
                            </div>
                            
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </form>
                    </div>
                </div>
            </div>
    </div>
</main>
@endsection

@section('footer')
    @include('layouts.partials.footer')
@endsection