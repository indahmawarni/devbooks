@extends('layouts.master')

@section('title', 'Devbooks')
@section('topnav')
    @include('layouts.partials.topnav')
@endsection

@section('sidenav')
    @include('layouts.partials.sidenav')
@endsection

@section('content')
<main>
    
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        
        <!-- Content Row -->
    <div class="row">
        <!-- Total Pengguna -->
        <div class="col-xl-3 col-md-6 mb-4 square-first">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-1">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Total Pengguna Aktif</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $total }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-chart-bar fa-3x"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Subkoordinator -->
        <div class="col-xl-3 col-md-6 mb-4 square-second">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                Subkoordinator</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $subkoordinator }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-chart-bar fa-3x"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Koordinator -->
        <div class="col-xl-3 col-md-6 mb-4 square-third">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Koordinator
                            </div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{ $koordinator }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-chart-bar fa-3x"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Direktur -->
        <div class="col-xl-3 col-md-6 mb-4 square-fourth">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                Direktur</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $direktur }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-chart-bar fa-3x"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

        @if (Auth::user()->role_id == 1)
        <a href="{{ url('tambah-pengguna')}}" class="btn btn-info mt-3" role="button"><i class="fas fa-plus-circle mr-2"></i>Tambah Pengguna</a>
        @endif    
        <br><br>
            @if(session('sukses'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Berhasil!</strong>
                {{session('sukses')}}
               
            </div>
            @endif
            
        <div class="card my-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                Daftar Pengguna
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Username</th>
                <th>Email</th>
                <th>Role</th>
                <th>Unit Kerja</th>
                <th>Aksi</th>
                <th>Status</th>
            </tr>
         
        </thead>
        <tbody>
            @foreach ($datauser as $user)
            <tr>
                <td>{{ $user->nama }}</td>
                <td>{{ $user->username }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->role['name']}}</td>
                <td>{{ $user->unitkerja['nama']}}</td>

                @if($user->deleted_at == null)
                <td> 
                    <a class="btn btn-info btn-sm" href="{{ url('profil/admin/edit/'.$user->id) }}" role="button"><i class="fas fa-edit"></i> Ubah</a>     
                    </a>
                    <a href="{{ url('hapus-pengguna').'/'.$user->id }}" type="button" class="btn btn-danger btn-sm">Nonaktifkan</a>           
                </td>
                <td>Aktif</td>
                @else 
                <td> 
                    <a class="btn btn-info btn-sm" href="{{ url('profil/admin/edit/'.$user->id) }}" role="button"><i class="fas fa-edit"></i> Ubah</a>     
                    </a>        
                    <a href="{{ url('aktif-pengguna').'/'.$user->id }}" type="button" class="btn btn-success btn-sm">Aktifkan</a>                 
                </td>
                <td>Nonaktif</td>
                @endif 
  
            </tr>
            @endforeach
            
        </tbody>
        
    </table>
                </div>
            </div>
        </div>
    </div>
</main>
@include('sweetalert::alert')
@endsection

@section('footer')
    @include('layouts.partials.footer')
@endsection