@extends('layouts.masterlogin')

@section('title', 'Devbooks')



@section('content')
<div class="body-login">
<main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
    <div class="container">
      <div class="card login-card">
        <div class="row no-gutters">
          <div class="col-md-5">
            <img src="image/login.svg" alt="login" class="login-card-img">
          </div>
          <div class="col-md-7">
            <div class="card-body">
              <div class="brand-wrapper">
                <img src="image/devbookshitam.png" alt="logo" class="logo">
              </div>
              <form action="{{route('login')}}" method="POST">
              {{csrf_field()}}
              @if (session('message'))
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <span></span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <strong></strong>
                  <span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
              </div>
              @endif
                  <div class="form-group">
                    <label for="email" class="sr-only">Username</label>
                    <input type="username" name="username" id="signin-username"  class="form-control" placeholder="Username" required>
                  </div>
                  <div class="form-group mb-0">
                    <label for="password" class="sr-only">Password</label>
                    <input type="password" name="password" id="myInput"  class="form-control" placeholder="Password" required>
                    <input type="checkbox" onclick="myFunction()"> Show Password
                  </div>
                  <div class="form-group text-right">
                    {{-- <label for="email" class="sr-only">Username</label>
                      <a href="{{ url('/forgot_password') }}">Lupa Password?</a> --}}
                      @if (Route::has('password.request'))
                      <a class="btn btn-link" href="{{ route('password.request') }}">
                          {{ __('Lupa Password?') }}
                      </a>
                  @endif
                  </div>
                  <input name="login" id="login" class="btn btn-block login-btn mb-4" type="submit" value="Login">
                 
                </form>
               
            </div>
          </div>
        </div>
      </div>
     
    </div>
  </main>
</div>
       
  <script>
    function myFunction() {
      var x = document.getElementById("myInput");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
    </script>
@endsection
