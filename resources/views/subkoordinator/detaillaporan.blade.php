@extends('layouts.master')

@section('title', 'Dashboard')
@section('topnav')
    @include('layouts.partials.topnav')
@endsection

@section('sidenav')
    @include('layouts.partials.sidenav')
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">User Requirement</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Detail</li>
        </ol>
        
        <div class="card my-4">
            <div class="card-header">
                Detail Laporan
            </div>
            <br><br>
            <div class="container">
            <div class="card-body">
                <div class="detail table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                          <tr >
                            <th scope="col" class="detail">Tanggal</th>
                            <td> {{ $laporan->tgl_laporan }} </td>
                          </tr>
                          <tr>
                            <th scope="col" class="detail">Judul</th>
                            <td>{{ $laporan->judul }}</td>
                          </tr>
                          <tr>
                            <th scope="col" class="detail">Jenis</th>
                            <td>{{ $laporan->jenis }}</td>
                          </tr>
                          <tr>
                            <th scope="col" class="detail">Isi</th>
                            <td>{!! $laporan->isi !!}</td>
                          </tr>
                          <tr>
                            <th scope="col" class="detail">Kesimpulan/Saran</th>
                            <td>{{ $laporan->kesimpulan }}</td>
                          </tr>
                        </tbody>
                    </table>
                    <div class="form-group mt-5 catatan">
                        <label for="catatan" class="col-form-label"><b>Catatan ke Subkoordinator</b></label>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                  <tr >
                                    <td class="catatan"> {{ $laporan->catatan }} </td>
                                  </tr>
                            </table>
                           
                        </div>
                      
                        <div class="row justify-content-sm-center mb-5">
                            
                            <a class="btn btn-info" href="{{ url('mengubah-subkoordinator/'.$laporan->id.'/edit') }}" role="button"><i class="fas fa-edit"></i> Ubah</a>
                          
                            <button type="submit" class="btn btn-info ml-2"><i class="fas fa-paper-plane"></i> Kirim ke Atasan</button>
                        </div>
                        
                    </div> 
                </div>
            </div>
        </div>
        </div>
    </div>
</main>
@endsection

@section('footer')
    @include('layouts.partials.footer')
@endsection



<!-- Button trigger modal -->
<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#ModalTambah"><i class="fas fa-plus-circle mr-2"></i>
  Tambah
</button>
<!-- End Button trigger modal -->