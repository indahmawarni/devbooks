@extends('layouts.master')

@section('title', 'Dashboard')
@section('topnav')
    @include('layouts.partials.topnav')
@endsection

@section('sidenav')
    @include('layouts.partials.sidenav')
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        
                {{-- <option value="{{$role->id}}">{{$role->name}}</option> --}}
        
        <form action="{{route('laporan-index')}}" method="get">
            <div class="form-group row">
                <select name="id_tahapan" id="" class="form-control">
                    <option selected disabled">Pilih Tahapan</option>
                    @foreach($tahapans as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group row">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
            
        </form>
        
        <br><br>
            @if(session('sukses'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Berhasil!</strong>
                {{session('sukses')}}
               
            </div>
               
            @endif
        <div class="card my-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                Daftar Laporan
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Judul</th>
                <th>Jenis</th>
                <th>Versi</th>
                <th>Posisi</th>
                <th>Status</th>
                <th>Tahapan</th>
                <th>Aksi</th>
                
                
            </tr>
         
        </thead>
        <tbody>
            @foreach ($laporan as $datalaporan)
                <tr>
                    <td>{{ $datalaporan->tgl_laporan }}</td>
                    <td>{{ $datalaporan->judul }}</td>
                    <td>{{ $datalaporan->jenis }}</td>
                    <td>{{ $datalaporan->versi }}</td>
                    <td>{{ $datalaporan->posisi }}</td>
                    <td>{{ $datalaporan->status }}</td>
                    <td>{{$datalaporan->tahapan['nama']}}</td>
                    <td> 


                        {{-- <a class="btn btn-info btn-sm" href="{{ url('mengubah-laporan/'.$datalaporan->id.'/edit') }}" role="button"><i class="fas fa-edit"></i> Ubah</a> --}}
                        <a class="btn btn-success btn-sm" href="{{ url('mengubah-laporan/'.$datalaporan->id.'/edit') }}" role="button"><i class="far fa-file-alt"></i> Hasil Generate</a>
                        <a class="btn btn-warning btn-sm" href="{{ url('detail-subkoordinator/'.$datalaporan->id) }}"><i class="far fa-file-alt"></i> Detail</a>
                        
                        

                        
                        <!-- Button trigger modal -->
                        
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModal"><i class="far fa-trash-alt"></i>
                                Hapus
                            </button>
                        </div>
                            
                        <!-- Modal Hapus-->
                            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Hapus Laporan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                        Apakah Anda yakin ingin menghapus?
                                        </div>
                                        <div class="modal-footer">
                                            <form action="{{ url('hapus-laporan/'.$datalaporan->id) }}"  method="POST">
                                            @method('delete')
                                            @csrf      
                                            <button class="btn btn-danger">Hapus
                                            </button>
                                            </form>  
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {{-- <div class="btn-group">
                            Detail
                            </button>
                            <div class="dropdown-menu">
                                @foreach ($tahapans as $tahapan)
                                <a class="dropdown-item" value="{{$tahapan->id}}" href="{{ url('detail/'.$datalaporan->id) }}">{{$tahapan->nama}}</a>
                                @endforeach
                            </div>
                        </div>  --}}
                    </td>   
                </tr>
            @endforeach
            
        </tbody>
        
    </table>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('footer')
    @include('layouts.partials.footer')
@endsection