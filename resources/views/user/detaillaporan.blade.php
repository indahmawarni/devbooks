@extends('layouts.master')


@section('title', 'Devbooks')



@section('topnav')
    @include('layouts.partials.topnav')
@endsection

@section('sidenav')
    @include('layouts.partials.sidenav')
@endsection

@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">{{ $laporan->nama_aplikasi }}</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Detail</li>
            </ol>
        </div>

        {{-- Step Wizard --}}
        <div class="mt-5">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="nav nav-pills justify-content-center mb-3 text-center custom" id="pills-tab"
                            role="tablist">
                            <div class="step-line"></div>
                            <li class="nav-item">
                                <a class="nav-link active custom" id="pills-1-tab" data-toggle="pill" href="#pills-1"
                                    role="tab" aria-controls="pills-1" aria-selected="true">
                                    <span class="icon"><i class="bi bi-person-circle"></i></span>
                                    <span class="text">User Requirement</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                @if ($laporan->nama_tahapan == 'User Requirement')
                                <a class="nav-link custom disabled" id="pills-2-tab" data-toggle="pill" href="#pills-2" role="tab"
                                    aria-controls="pills-2" aria-selected="false">
                                    <span class="icon"><i class="bi bi-file-earmark-code-fill"></i></span>
                                    <span class="text">Pengembangan</span>
                                </a>
                                @else
                                <a class="nav-link custom" id="pills-2-tab" data-toggle="pill" href="#pills-2" role="tab"
                                    aria-controls="pills-2" aria-selected="false">
                                    <span class="icon"><i class="bi bi-file-earmark-code-fill"></i></span>
                                    <span class="text">Pengembangan</span>
                                </a>
                                @endif
                            </li>   
                            <li class="nav-item">
                                @if ($laporan->nama_tahapan == 'User Requirement' || $laporan->nama_tahapan == 'Pengembangan')
                                <a class="nav-link custom disabled" id="pills-3-tab" data-toggle="pill" href="#pills-3"
                                    role="tab" aria-controls="pills-3" aria-selected="false">
                                    <span class="icon"><i class="bi bi-check-circle-fill"></i></span>
                                    <span class="text">Testing</span>
                                </a>
                                @else
                                <a class="nav-link custom" id="pills-3-tab" data-toggle="pill" href="#pills-3"
                                    role="tab" aria-controls="pills-3" aria-selected="false">
                                    <span class="icon"><i class="bi bi-check-circle-fill"></i></span>
                                    <span class="text">Testing</span>
                                </a>
                                @endif
                            </li>
                            <li class="nav-item">
                                @if ($laporan->nama_tahapan == 'User Requirement' || $laporan->nama_tahapan == 'Pengembangan' || $laporan->nama_tahapan == 'Testing')
                                <a class="nav-link custom disabled" id="pills-4-tab" data-toggle="pill" href="#pills-4" role="tab"
                                    aria-controls="pills-4" aria-selected="false">
                                    <span class="icon"><i class="bi bi-menu-button-wide-fill"></i></span>
                                    <span class="text">Rilis</span>
                                </a>
                                @else
                                <a class="nav-link custom" id="pills-4-tab" data-toggle="pill" href="#pills-4" role="tab"
                                    aria-controls="pills-4" aria-selected="false">
                                    <span class="icon"><i class="bi bi-menu-button-wide-fill"></i></span>
                                    <span class="text">Rilis</span>
                                </a>
                                @endif

                            </li>
                        </ul>
                        <div class="tab-content custom pt-4" id="pills-tabContent">

                            <!-- STEP 1 -->
                            <div class="tab-pane fade show active" id="pills-1" role="tabpanel" aria-labelledby="pills-home-tab">
                                    
                                <form action="{{ url('requirement-kirim/'. $laporan_id) }}" method="POST" class="form-requirement"> 
                                    @csrf
                                    <!-- Button trigger modal -->
                                    @if ((Auth::user()->role_id == 2 && $laporan->posisi == 'Subkoordinator' && $laporan->nama_tahapan == 'User Requirement') || (Auth::user()->role_id == 3 && $laporan->posisi == 'Koordinator' && $laporan->nama_tahapan == 'User Requirement') || (Auth::user()->role_id == 4 && $laporan->posisi == 'Direktur' && $laporan->nama_tahapan == 'User Requirement'))
                                    <div class="row">
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                                            data-target="#ModalTambah"><i class="fas fa-plus-circle mr-2"></i>
                                            Tambah Fitur
                                        </button>
                                    </div>
                                    @endif
                                    @if ($laporan->nama_tahapan !== 'User Requirement' && $laporan->tgl_requirement !== null)
                                    <div class="row">
                                        <a class="btn btn-success btn-sm ml-auto"
                                            href="{{ url('generate/requirement/'.$laporan->id) }}" role="button"
                                            aria-disabled="true"><i class="far fa-file-alt"></i> Laporan User Requirement
                                        </a>
                                    </div>     
                                    @else
                                    <div class="row">
                                        <a class="btn btn-success disabled btn-sm ml-auto"
                                            href="{{ url('generate/requirement/'.$laporan->id) }}" role="button"
                                            aria-disabled="true"><i class="far fa-file-alt"></i> Laporan User Requirement
                                        </a>
                                    </div>     
                                    @endif
                                    <!-- End Button trigger modal -->
                                    <h5 class="text-center">Daftar Fitur</h5>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col" class="text-center">No</th>
                                                <th scope="col" class="text-center">Nama Fitur</th>
                                                
                                                <th scope="col" class="text-center">User Requirement</th>
                                                @if ((Auth::user()->role_id == 2 && $laporan->posisi == 'Subkoordinator' && $laporan->nama_tahapan == 'User Requirement') || (Auth::user()->role_id == 3 && $laporan->posisi == 'Koordinator' && $laporan->nama_tahapan == 'User Requirement') || (Auth::user()->role_id == 4 && $laporan->posisi == 'Direktur' && $laporan->nama_tahapan == 'User Requirement'))
                                                <th scope="col" class="text-center">Keterangan</th>
                                                {{-- <th scope="col">Role</th> --}}
                                                <th scope="col" class="text-center">Aksi</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($requirement as $item)
                                                <tr>
                                                    <td class="data">
                                                        <div class="container" style="text-align:center">
                                                        {{ $loop->iteration }}
                                                        </div>
                                                    </td>
                                                    <td class="data">{{ $item->nama_fitur }}</td>
                                                    
                                                    <td>
                                                        <div class="container" style="text-align:center">
                                                            <input type="checkbox" name="status_requirement[{{ $item->id }}]"
                                                            @if ($item->status_requirement == '1')
                                                            checked
                                                            @endif
                                                            >
                                                            </div>
                                                        <input type="hidden" name="role_id[{{ $item->id }}]" value="{{Auth::user()->role_id
                                                        }}">
                                                    </td>
                                                    @if ((Auth::user()->role_id == 2 && $laporan->posisi == 'Subkoordinator' && $laporan->nama_tahapan == 'User Requirement') || (Auth::user()->role_id == 3 && $laporan->posisi == 'Koordinator' && $laporan->nama_tahapan == 'User Requirement') || (Auth::user()->role_id == 4 && $laporan->posisi == 'Direktur' && $laporan->nama_tahapan == 'User Requirement'))
                                                    <td>
                                                        <textarea id="" class="form-control" name="catatan_requirement[{{ $item->id }}]"  cols="30" rows="3">{{$item->catatan_requirement}}</textarea> 
                                                        <input type="button" value="Bersihkan" onclick="javascript:eraseTextRequirement('catatan_requirement[{{ $item->id }}]')"> 
                                                          
                                                    </td>
                                                    {{-- <td class="data">
                                                        @if ($item->role_id != 0 )
                                                            @if ($item->role_id == 2)
                                                                Subkoordinator
                                                            @elseif ($item->role_id == 3)
                                                                Koordinator
                                                            @elseif ($item->role_id == 4)
                                                                Direktur
                                                            @endif   
                                                        @else
                                                        tidak ada 
                                                        @endif
                                                    </td> --}}
                                                    <td>
                                                        <div class="container" style="text-align:center">
                                                            <div class="btn-group">
                                                                <a href="" type="button" class="btn btn-sm btn-success"
                                                                    data-toggle="modal"
                                                                    data-target="#modalUbahFitur-{{ $item->id }}"><i
                                                                        class="fas fa-edit"></i>
                                                                </a>
                                                            </div>
                                                            <div class="btn-group">
                                                                <a href="" type="button" class="btn btn-sm btn-danger"
                                                                    data-toggle="modal"
                                                                    data-target="#modalHapus-{{ $item->id }}"><i
                                                                        class="far fa-trash-alt"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table><br>
                                    
                                    <!-- Button trigger modal -->
                                    <div class="form-group">
                                        <label for="" class="required"><b>Tanggal</b></label>
                                        <input name="tgl_requirement" class="form-control" type="date" id="inputTanggal" value="{{ $laporan->tgl_requirement }}" required>
                                    </div>
                                    <label for=""><b>Kesimpulan</b></label> <br>
                                    <textarea name="kesimpulan_requirement" id="kesimpulanRequirement" class="form-control"
                                        >
                                        {{ $laporan->kesimpulan_requirement }}   
                                    </textarea>
                                    <script>
                                        tinymce.init({
                                            selector: '#kesimpulanRequirement',
                                            height: 350,
                                            plugins: 'table',
                                            table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
                                            content_style: "body { font-family: Arial; }",
                                        })
                                    </script>
                                    <br><br>
                                    <!-- End Button trigger modal -->
                                    {{-- <table class="table table-borderless mb-5">
                                        <tbody>
                                            <tr>
                                                <th scope="col" class="detail">Tanggal &emsp; &emsp; &emsp; &emsp; &emsp; :</th>
                                            </tr>
                                            <tr>
                                                @if ($laporan->tgl_requirement !==null)
                                                <td>{{ \Carbon\Carbon::parse($laporan->tgl_requirement)->isoFormat('dddd, D MMMM Y') }}
                                                </td>
                                                @endif                   
                                            </tr>
                                            <tr>
                                                <th scope="col"></th>
                                            </tr>
                                            <tr></tr>
                                            <tr>
                                                <th scope="col" class="detail">Kesimpulan/Saran &emsp; :</th>
                                            </tr>
                                            <tr>
                                                <td>{!! $laporan->kesimpulan_requirement !!}</td>
                                            </tr>
                                        </tbody>
                                    </table> --}}
                                    @if ((Auth::user()->role_id == 2 && $laporan->posisi == 'Subkoordinator' && $laporan->nama_tahapan == 'User Requirement') || (Auth::user()->role_id == 3 && $laporan->posisi == 'Koordinator' && $laporan->nama_tahapan == 'User Requirement'))
                                    <div class="row justify-content-sm-center">
                                            <div class="" id="form-kirim"></div>
                                            <button type="submit" class="btn btn-info ml-2"><i class="fas fa-paper-plane"></i>
                                                Kirim ke Atasan</button>
                                        {{-- </form> --}}
                                    </div>
                                    @elseif((Auth::user()->role_id == 4 && $laporan->posisi == 'Direktur' && $laporan->nama_tahapan == 'User Requirement'))
                                    <div class="row justify-content-sm-center">
                                        {{-- <form action="{{ url('requirement-kirim/'. $laporan_id)}}" method="POST"> --}}
                                        <button type="submit" class="btn btn-info ml-2"><i class="fas fa-paper-plane"></i> Setujui</button>
                                    </div>  
                                    @endif
                                    <br><br>
                                </form>
                                </div>
                            <!-- End Step 1 -->


                            <!-- STEP 2 -->
                            <div class="tab-pane fade" id="pills-2" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <form action="{{ url('pengembangan-kirim/'. $laporan_id) }}" method="POST" class="form-pengembangan">
                                    @csrf
                                    @if ($laporan->nama_tahapan !== 'Pengembangan' && $laporan->tgl_pengembangan !== null)
                                    <div class="row">
                                        <a class="btn btn-success btn-sm ml-auto"
                                            href="{{ url('generate/pengembangan/'.$laporan->id) }}" role="button"
                                            aria-disabled="true"><i class="far fa-file-alt"></i> Laporan Pengembangan
                                        </a>
                                    </div>                                        
                                    @else
                                    <div class="row">
                                        <a class="btn btn-success disabled btn-sm ml-auto"
                                            href="{{ url('generate/pengembangan/'.$laporan->id) }}" role="button"
                                            aria-disabled="true"><i class="far fa-file-alt"></i> Laporan Pengembangan
                                        </a>
                                    </div>  
                                    @endif
                                    <!-- End Button trigger modal -->
                                    <h5 class="text-center">Daftar Fitur</h5>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col" class="text-center">No</th>
                                                <th scope="col" class="text-center">Nama Fitur</th>                                        
                                                <th scope="col" class="text-center">User Requirement</th> 
                                                <th scope="col" class="text-center">Pengembangan</th>
                                                @if ((Auth::user()->role_id == 2 && $laporan->posisi == 'Subkoordinator' && $laporan->nama_tahapan == 'Pengembangan') || (Auth::user()->role_id == 3 && $laporan->posisi == 'Koordinator' && $laporan->nama_tahapan == 'Pengembangan') || (Auth::user()->role_id == 4 && $laporan->posisi == 'Direktur' && $laporan->nama_tahapan == 'Pengembangan'))
                                                <th scope="col" class="text-center">Keterangan</th>
                                                @endif
                                           
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            @foreach ($pengembangan as $item)
                                                <tr>
                                                    <td class="data">
                                                        <div class="container" style="text-align:center">
                                                        {{ $loop->iteration }}
                                                        </div>
                                                    </td>
                                                    <td class="data">{{ $item->nama_fitur }}</td>
                                                
                                                    <td>
                                                        <div class="container" style="text-align:center">
                                                        <i class="fa fa-check" style="font-size:18px; color:green; text-align:center"></i>
                                                        {{-- <i class="fa fa-close" style="font-size:24px"></i> --}}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="container" style="text-align:center">
                                                            <input type="checkbox" name="status_pengembangan[{{ $item->id }}]"
                                                            @if ($item->status_pengembangan == '1')
                                                            checked
                                                            @endif
                                                            >
                                                        <div>
                                                    </td>
                                                    @if ((Auth::user()->role_id == 2 && $laporan->posisi == 'Subkoordinator' && $laporan->nama_tahapan == 'Pengembangan') || (Auth::user()->role_id == 3 && $laporan->posisi == 'Koordinator' && $laporan->nama_tahapan == 'Pengembangan') || (Auth::user()->role_id == 4 && $laporan->posisi == 'Direktur' && $laporan->nama_tahapan == 'Pengembangan'))
                                                    <td>
                                                        <textarea class="form-control" name="catatan_pengembangan[{{ $item->id }}]" id="" cols="30" rows="3" id='output'>{{$item->catatan_pengembangan}}</textarea>   
                                                        <input type="button" value="Bersihkan" onclick="javascript:eraseTextPengembangan('catatan_pengembangan[{{ $item->id }}]');"> 
                                                    </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table><br>
                                    
                                    <!-- Button trigger modal -->
                                    <div class="form-group">
                                        <label for="" class="required"><b>Tanggal</b></label>
                                        <input name="tgl_pengembangan" class="form-control" type="date" id="inputTanggal" value="{{ $laporan->tgl_pengembangan }}" required>
                                    </div>
                                    <label for=""><b>Kesimpulan</b></label> <br>
                                    <textarea name="kesimpulan_pengembangan" id="kesimpulanPengembangan" class="form-control"
                                        >
                                        {{ $laporan->kesimpulan_pengembangan }}   
                                    </textarea>
                                    <script>
                                        tinymce.init({
                                            selector: '#kesimpulanPengembangan',
                                            height: 350,
                                            plugins: 'table',
                                            table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
                                            content_style: "body { font-family: Arial; }",
                                        })
                                    </script>
                                    <br><br>
                                    {{-- @if ($laporan->kesimpulan_pengembangan == null && Auth::user()->role_id == 2)
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                                            data-target="#tambahLaporanPengembangan"><i class="fas fa-plus-circle mr-2"></i>
                                            Tambah Laporan
                                        </button>
                                    @endif
                                    <br><br>
                                    <!-- End Button trigger modal -->
                                    <table class="table table-borderless mb-5">
                                        <tbody>
                                            <tr>
                                                <th scope="col" class="detail">Tanggal &emsp; &emsp; &emsp; &emsp; &emsp; :</th>
                                            </tr>
                                            <tr>
                                                @if ($laporan->tgl_pengembangan !==null)
                                                <td>{{ \Carbon\Carbon::parse($laporan->tgl_pengembangan)->isoFormat('dddd, D MMMM Y') }}
                                                </td>
                                                @endif                   
                                            </tr>
                                            <tr>
                                                <th scope="col"></th>
                                            </tr>
                                            <tr></tr>
                                            <tr>
                                                <th scope="col" class="detail">Kesimpulan/Saran &emsp; :</th>
                                            </tr>
                                            <tr>
                                                <td>{!! $laporan->kesimpulan_pengembangan !!}</td>
                                            </tr>
                                        </tbody>
                                    </table> --}}
                                    @if ((Auth::user()->role_id == 2 && $laporan->posisi == 'Subkoordinator' && $laporan->nama_tahapan == 'Pengembangan') || (Auth::user()->role_id == 3 && $laporan->posisi == 'Koordinator' && $laporan->nama_tahapan == 'Pengembangan'))
                                    <div class="row justify-content-sm-center">
                                            <div class="" id="form-kirim"></div>
                                            <button type="submit" class="btn btn-info ml-2"><i class="fas fa-paper-plane"></i>
                                                Kirim ke Atasan</button>
                                        {{-- </form> --}}
                                    </div>
                                    @elseif((Auth::user()->role_id == 4 && $laporan->posisi == 'Direktur' && $laporan->nama_tahapan == 'Pengembangan'))
                                    <div class="row justify-content-sm-center">
                   {{-- <form action="{{ url('pengembangan-kirim/'. $laporan_id)}}" method="POST">
                                        @method('POST')
                                        {{ csrf_field() }} --}}
                                        <button type="submit" class="btn btn-info ml-2"><i class="fas fa-paper-plane"></i> Setujui</button>
                                    </div>  
                                    @endif
                                    <br><br>
                                </form>
                            </div>
                            <!-- End Step 2 -->

                            {{-- STEP 3 --}}
                            <div class="tab-pane fade" id="pills-3" role="tabpanel" aria-labelledby="pills-contact-tab">
                                <form action="{{ url('testing-kirim/'. $laporan_id) }}" method="POST" class="form-testing">
                                    {{ csrf_field() }}
                                @if ($laporan->nama_tahapan !== 'Testing' && $laporan->tgl_testing !== null)
                                <div class="row">
                                    <a class="btn btn-success btn-sm ml-auto"
                                        href="{{ url('generate/testing/'.$laporan->id) }}" role="button"
                                        aria-disabled="true"><i class="far fa-file-alt"></i> Laporan Testing
                                    </a>
                                </div>
                                @else
                                <div class="row">
                                    <a class="btn btn-success disabled btn-sm ml-auto"
                                        href="{{ url('generate/testing/'.$laporan->id) }}" role="button"
                                        aria-disabled="true"><i class="far fa-file-alt"></i> Laporan Testing
                                    </a>
                                </div>
                                @endif
                                <!-- End Button trigger modal -->
                                <h5 class="text-center">Daftar Fitur</h5>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col" class="text-center">No</th>
                                            <th scope="col" class="text-center">Nama Fitur</th>
                                            {{-- @if ((Auth::user()->role_id == 2 && $laporan->posisi == 'Subkoordinator' && $laporan->nama_tahapan == 'Testing') || (Auth::user()->role_id == 3 && $laporan->posisi == 'Koordinator' && $laporan->nama_tahapan == 'Testing') || (Auth::user()->role_id == 4 && $laporan->posisi == 'Direktur' && $laporan->nama_tahapan == 'Testing')) --}}
                                            <th scope="col" class="text-center">User Requirement</th>
                                            <th scope="col" class="text-center">Pengembangan</th>
                                            <th scope="col" class="text-center">Testing</th>
                                            @if ((Auth::user()->role_id == 2 && $laporan->posisi == 'Subkoordinator' && $laporan->nama_tahapan == 'Testing') || (Auth::user()->role_id == 3 && $laporan->posisi == 'Koordinator' && $laporan->nama_tahapan == 'Testing') || (Auth::user()->role_id == 4 && $laporan->posisi == 'Direktur' && $laporan->nama_tahapan == 'Testing'))
                                            <th scope="col" class="text-center">Keterangan</th>
                                            @endif
                                            {{-- @endif --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($testing as $item)
                                            <tr>
                                                <td class="data">
                                                    <div class="container" style="text-align:center">
                                                    {{ $loop->iteration }}
                                                    </div>
                                                </td>
                                                <td class="data">{{ $item->nama_fitur }}</td>
                                                <td>
                                                    <div class="container" style="text-align:center">
                                                    @if ($item->status_requirement == '1')
                                                        <i class="fa fa-check" style="font-size:18px; color:green; text-align:center"></i>
                                                    @endif
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="container" style="text-align:center">
                                                    @if ($item->status_pengembangan == '1')
                                                        <i class="fa fa-check" style="font-size:18px; color:green; text-align:center"></i>
                                                    @elseif ($item->status_pengembangan == '0')
                                                        <span class="badge badge-pill badge-warning">Fitur tidak dikembangkan</span>
                                                    @endif
                                                    </div>
                                                </td>
                                                
                                                <td>
                                                    <div class="container" style="text-align:center">
                                                    @if ($item->status_pengembangan == "1")
                                                        
                                                            <input type="checkbox" name="status_testing[{{ $item->id }}]"
                                                            @if ($item->status_testing == '1')
                                                            checked
                                                            @endif
                                                            >
                                                        @else
                                                        <span class="badge badge-pill badge-warning">Fitur tidak dikembangkan</span>
                                                        @endif
                                                    <div>
                                                    
                                                </td>
                                                @if ((Auth::user()->role_id == 2 && $laporan->posisi == 'Subkoordinator' && $laporan->nama_tahapan == 'Testing') || (Auth::user()->role_id == 3 && $laporan->posisi == 'Koordinator' && $laporan->nama_tahapan == 'Testing') || (Auth::user()->role_id == 4 && $laporan->posisi == 'Direktur' && $laporan->nama_tahapan == 'Testing'))
                                                <td>
                                                    <textarea class="form-control clear-testing" name="catatan_testing[{{ $item->id }}]" id="" cols="30" rows="3">{{$item->catatan_testing}} </textarea>  
                                                    <input type="button" value="Bersihkan" onclick="javascript:eraseTextTesting('catatan_testing[{{ $item->id }}]');">  
                                                </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table><br>
                                <!-- Button trigger modal -->
                                <div class="form-group">
                                    <label for="" class="required"><b>Tanggal</b></label>
                                    <input name="tgl_testing" class="form-control" type="date" id="inputTanggal" value="{{ $laporan->tgl_testing }}" required>
                                </div>
                                <label for=""><b>Kesimpulan</b></label> <br>
                                <textarea name="kesimpulan_testing" id="kesimpulanTesting" class="form-control"
                                    >
                                    {{ $laporan->kesimpulan_testing }}   
                                </textarea>
                                <script>
                                    tinymce.init({
                                        selector: '#kesimpulanTesting',
                                        height: 350,
                                        plugins: 'table',
                                        table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
                                        content_style: "body { font-family: Arial; }",
                                    })
                                </script>
                                <br><br>
                                {{-- @if ($laporan->kesimpulan_testing == null && Auth::user()->role_id == 2)
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                                        data-target="#tambahLaporanTesting"><i class="fas fa-plus-circle mr-2"></i>
                                        Tambah Laporan
                                    </button>
                                @endif
                                <br><br>
                                <!-- End Button trigger modal -->
                                <table class="table table-borderless mb-5">
                                    <tbody>
                                        <tr>
                                            <th scope="col" class="detail">Tanggal &emsp; &emsp; &emsp; &emsp; &emsp; :</th>
                                        </tr>
                                        <tr>
                                            @if ($laporan->tgl_testing !==null)
                                            <td>{{ \Carbon\Carbon::parse($laporan->tgl_testing)->isoFormat('dddd, D MMMM Y') }}
                                            </td>
                                            @endif                   
                                        </tr>
                                        <tr>
                                            <th scope="col"></th>
                                        </tr>
                                        <tr></tr>
                                        <tr>
                                            <th scope="col" class="detail">Kesimpulan/Saran &emsp; :</th>
                                        </tr>
                                        <tr>
                                            <td>{!! $laporan->kesimpulan_testing !!}</td>
                                        </tr>
                                    </tbody>
                                </table> --}}
                                @if ((Auth::user()->role_id == 2 && $laporan->posisi == 'Subkoordinator' && $laporan->nama_tahapan == 'Testing') || (Auth::user()->role_id == 3 && $laporan->posisi == 'Koordinator' && $laporan->nama_tahapan == 'Testing'))
                                <div class="row justify-content-sm-center">
       {{-- <form action="{{ url('testing-kirim/'. $laporan_id) }}" method="POST">
                                        @method('POST')
                                        {{ csrf_field() }} --}}
                                        <div class="" id="form-kirim"></div>
                                        <button type="submit" class="btn btn-info ml-2"><i class="fas fa-paper-plane"></i>
                                            Kirim ke Atasan</button>
                                
                                </div>
                                @elseif((Auth::user()->role_id == 4 && $laporan->posisi == 'Direktur' && $laporan->nama_tahapan == 'Testing'))
                                <div class="row justify-content-sm-center">
                  {{-- <form action="{{ url('testing-kirim/'. $laporan_id)}}" method="POST">
                                    @method('POST')
                                    {{ csrf_field() }} --}}
                                    <button type="submit" class="btn btn-info ml-2"><i class="fas fa-paper-plane"></i> Setujui</button>
                                    </form>
                                </div>  
                                @endif
                                <br><br>
                            </form>
                            </div>
                            {{-- End Step 3 --}}

                            {{-- STEP 4 --}}
                            <div class="tab-pane fade" id="pills-4" role="tabpanel" aria-labelledby="pills-contact-tab">
                                <form action="{{ url('rilis-kirim/'. $laporan_id) }}" method="POST" class="form-rilis">
                                    {{ csrf_field() }}
                                @if ($laporan->nama_tahapan !== 'Rilis' && $laporan->tgl_rilis !== null)
                                <div class="row">
                                    <a class="btn btn-success btn-sm ml-auto"
                                        href="{{ url('generate/rilis/'.$laporan->id) }}" role="button"
                                        aria-disabled="true"><i class="far fa-file-alt"></i> Laporan Rilis
                                    </a>
                                </div>
                                @else
                                <div class="row">
                                    <a class="btn btn-success disabled btn-sm ml-auto"
                                        href="{{ url('generate/rilis/'.$laporan->id) }}" role="button"
                                        aria-disabled="true"><i class="far fa-file-alt"></i> Laporan Rilis
                                    </a>
                                </div>
                                @endif
                                <!-- End Button trigger modal -->
                                <h5 class="text-center">Daftar Fitur</h5>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col" class="text-center">No</th>
                                            <th scope="col" class="text-center">Nama Fitur</th>
                                            <th scope="col" class="text-center">User Requirement</th>
                                            <th scope="col" class="text-center">Pengembangan</th>
                                            <th scope="col" class="text-center">Testing</th>
                                            <th scope="col" class="text-center">Rilis</th>
                                            @if ((Auth::user()->role_id == 2 && $laporan->posisi == 'Subkoordinator' && $laporan->nama_tahapan == 'Rilis') || (Auth::user()->role_id == 3 && $laporan->posisi == 'Koordinator' && $laporan->nama_tahapan == 'Rilis') || (Auth::user()->role_id == 4 && $laporan->posisi == 'Direktur' && $laporan->nama_tahapan == 'Rilis'))
                                            <th scope="col" class="text-center">Keterangan</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($rilis as $item)
                                            <tr>
                                                
                                                <td class="data">
                                                    <div class="container" style="text-align:center">
                                                    {{ $loop->iteration }}
                                                    </div>
                                                </td>
                                                
                                                <td class="data">{{ $item->nama_fitur }}</td>
                                                <td>
                                                    <div class="container" style="text-align:center">
                                                    @if ($item->status_requirement == '1')
                                                        <i class="fa fa-check" style="font-size:18px; color:green; text-align:center"></i>
                                                    @endif
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="container" style="text-align:center">
                                                    @if ($item->status_pengembangan == '1')
                                                        <i class="fa fa-check" style="font-size:18px; color:green; text-align:center"></i>
                                                    @elseif ($item->status_pengembangan == '0')
                                                    <span class="badge badge-pill badge-warning">Fitur tidak dikembangkan</span>
                                                    @endif
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="container" style="text-align:center">
                                                        @if ($item->status_testing == '1')
                                                            <i class="fa fa-check" style="font-size:18px; color:green; text-align:center"></i>
                                                        @elseif ($item->status_testing == '0')
                                                        <span class="badge badge-pill badge-warning">Belum berjalan dengan baik</span>
                                                        @endif
                                                    </div>
                                                </td>
                                                <td>     <div class="container" style="text-align:center">                              
                                                        <input type="checkbox" name="status_rilis[{{ $item->id }}]"
                                                        @if ($item->status_rilis == '1')
                                                        checked         
                                                        @elseif ($item->status_rilis != null)  
                                                        @endif
                                                        >
                                                        </div>
                                                        
                                                        {{-- @else
                                                        <span class="badge badge-warning">Fitur tidak dikembangkan</span> --}}
                                                    
                                                </td>
                                                @if ((Auth::user()->role_id == 2 && $laporan->posisi == 'Subkoordinator' && $laporan->nama_tahapan == 'Rilis') || (Auth::user()->role_id == 3 && $laporan->posisi == 'Koordinator' && $laporan->nama_tahapan == 'Rilis') || (Auth::user()->role_id == 4 && $laporan->posisi == 'Direktur' && $laporan->nama_tahapan == 'Rilis'))
                                                <td>
                                                    <textarea class="form-control clear-rilis" name="catatan_rilis[{{ $item->id }}]" id="" cols="30" rows="3">{{$item->catatan_rilis}}</textarea>
                                                    <input type="button" value="Bersihkan" onclick="javascript:eraseTextRilis('catatan_rilis[{{ $item->id }}]');">    
                                                </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table><br>
                                <!-- Button trigger modal -->
                                <div class="form-group">
                                    <label for="" class="required"><b>Tanggal</b></label>
                                    <input name="tgl_rilis" class="form-control" type="date" id="inputTanggal" value="{{ $laporan->tgl_rilis }}" required>
                                </div>
                                <label for=""><b>Kesimpulan</b></label> <br>
                                <textarea name="kesimpulan_rilis" id="kesimpulanRilis" class="form-control"
                                    >
                                    {{ $laporan->kesimpulan_rilis }}   
                                </textarea>
                                <script>
                                    tinymce.init({
                                        selector: '#kesimpulanRilis',
                                        height: 350,
                                        plugins: 'table',
                                        table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
                                        content_style: "body { font-family: Arial; }",
                                    })
                                </script>
                                <br><br>
                                {{-- @if ($laporan->kesimpulan_rilis == null && Auth::user()->role_id == 2)
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                                        data-target="#tambahLaporanRilis"><i class="fas fa-plus-circle mr-2"></i>
                                        Tambah Laporan
                                    </button>
                                @endif
                                <br><br>
                                <!-- End Button trigger modal -->
                                <table class="table table-borderless mb-5">
                                    <tbody>
                                        <tr>
                                            <th scope="col" class="detail">Tanggal &emsp; &emsp; &emsp; &emsp; &emsp; :</th>
                                        </tr>
                                        <tr>
                                            @if ($laporan->tgl_rilis !==null)
                                            <td>{{ \Carbon\Carbon::parse($laporan->tgl_rilis)->isoFormat('dddd, D MMMM Y') }}
                                            </td>
                                            @endif                   
                                        </tr>
                                        <tr>
                                            <th scope="col"></th>
                                        </tr>
                                        <tr></tr>
                                        <tr>
                                            <th scope="col" class="detail">Kesimpulan/Saran &emsp; :</th>
                                        </tr>
                                        <tr>
                                            <td>{!! $laporan->kesimpulan_rilis !!}</td>
                                        </tr>
                                    </tbody>
                                </table> --}}
                                @if ((Auth::user()->role_id == 2 && $laporan->posisi == 'Subkoordinator' && $laporan->nama_tahapan == 'Rilis') || (Auth::user()->role_id == 3 && $laporan->posisi == 'Koordinator' && $laporan->nama_tahapan == 'Rilis'))
                                <div class="row justify-content-sm-center">
                   {{-- <form action="{{ url('rilis-kirim/'. $laporan_id) }}" method="POST">
                                        @method('POST')
                                        {{ csrf_field() }} --}}
                                        <div class="" id="form-kirim"></div>
                                        <button type="submit" class="btn btn-info ml-2"><i class="fas fa-paper-plane"></i>
                                            Kirim ke Atasan</button>
                                
                                </div>
                                @elseif((Auth::user()->role_id == 4 && $laporan->posisi == 'Direktur' && $laporan->nama_tahapan == 'Rilis'))
                                <div class="row justify-content-sm-center">
         {{-- <form action="{{ url('rilis-kirim/'. $laporan_id)}}" method="POST">
                                    @method('POST')
                                    {{ csrf_field() }} --}}
                                    <button type="submit" class="btn btn-info ml-2"><i class="fas fa-paper-plane"></i> Setujui</button>
                                    </form>
                                </div>  
                                @endif
                                <br><br>
                            </form>
                            </div>
                        {{-- End Step 4 --}}
                    </div>
                </div>
            </div>
        </div>
        <!-- End Wizard -->

        <!-- Modal Tambah Data Fitur Requirement-->
        <div class="modal fade" id="ModalTambah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Nama Fitur</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ url('tambah-fitur') }}" method="POST" id="form-tambah">
                            {{ csrf_field() }}
                            <input type="text" name="status_requirement" class="d-none" value="1">
                            <div class="container1">
                                <div class="form-group-row">
                                    <button type="button" class="btn btn-success btn-md add_form_field col-md-1 mb-2"
                                        id="tambah"><i class="fas fa-plus col-md-1 ml-0 pl-0"></i></button>
                                   
                                    <input type="hidden" name="laporan_id" value="{{ $laporan_id }}">
                                    <input type="text" class="form-control col-md-9 mb-3" name="nama_fitur[]">
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal -->

        <!-- Modal Ubah Data Fitur -->
        @foreach ($requirement as $item)
            <div class="modal fade" id="modalUbahFitur-{{ $item->id }}" tabindex="-1"
                aria-labelledby="modalUbahFitur" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ubah Data</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form class="ubah" action="{{ url('/update-fitur/'.$item->id) }}" method="POST">
                                @method('POST')
                                {{ csrf_field() }}
                                <div class="container1">
                                    <div class="form-group-row">
                                        <input type="hidden" name="laporan_id" value="{{ $laporan_id }}">
                                        <input type="text" class="form-control col-md-9 mb-3" name="nama_fitur"
                                            value="{{ $item->nama_fitur }}">
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Modal Ubah Fitur -->

            <!-- Modal Hapus Fitur -->
            <div class="modal fade" id="modalHapus-{{ $item->id }}" tabindex="-1" aria-labelledby="modalHapus"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="modalHapus">Hapus Data</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Apakah Anda yakin ingin menghapus?
                        </div>
                        <div class="modal-footer">
                            <form class="delete" action="{{ url('/hapus-fitur/' . $item->id) }}" method="POST">
                                @method('POST')
                                <input type="hidden" name="laporan_id" value="{{ $laporan_id }}">
                                @csrf
                                <button class="btn btn-danger">Hapus
                                </button>
                            </form>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <!-- End Modal Hapus Data Fitur -->

        <!-- Modal Tambah Laporan Requirement -->
        {{-- <div class="modal fade" id="ModalTambahRequirement" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Laporan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ url('tambah-requirement/' . $laporan_id) }}" method="POST">
                            @method('POST')
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for=""><b>Tanggal</b></label>
                                <input name="tgl_requirement" class="form-control" type="date" id="inputTanggal" required>
                            </div>
                            <label for=""><b>Kesimpulan</b></label> <br>
                            <textarea name="kesimpulan_requirement" id="kesimpulanRequirement" class="form-control"
                                required>
                            </textarea>
                            <script>
                                tinymce.init({
                                    selector: '#kesimpulanRequirement',
                                    height: 350,
                                    plugins: 'table',
                                    table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
                                    content_style: "body { font-family: Arial; }",
                                })
                            </script>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        </form>
                    </div>
                </div>
            </div>
        </div> --}}
        <!-- End Modal -->

        <!-- Modal Ubah Laporan Requirement -->
        {{-- <div class="modal fade" id="ModalUbahRequirement" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Nama Fitur</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <form action="{{ url('tambah-requirement/' . $laporan_id) }}" method="POST">
                                @method('POST')
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="">Tanggal</label>
                                    <input name="tgl_requirement" class="form-control" type="date" id="inputTanggal"
                                        value="{{ $laporan->tgl_requirement }}" required>
                                </div>
                                <label for=""><b>Kesimpulan</b></label> <br>
                                <textarea name="kesimpulan_requirement" id="kesimpulanRequirement2" class="form-control"
                                    required>
                  {{ $laporan->kesimpulan_requirement }}
                </textarea>
                                <script>
                                    tinymce.init({
                                        selector: '#kesimpulanRequirement2',
                                        height: 350,
                                        plugins: 'table',
                                        table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
                                        content_style: "body { font-family: Arial; }",
                                    })
                                </script>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        <!-- End Modal -->


    <!-- PENGEMBANGAN -->

        <!-- Modal Tambah Laporan Pengembangan -->
        <div class="modal fade" id="tambahLaporanPengembangan" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Laporan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ url('tambah-pengembangan/'.$laporan_id) }}" method="POST">
                            @method('POST')
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="">Tanggal</label>
                                <input name="tgl_pengembangan" class="form-control" type="date" id="inputTanggal" required>
                            </div>
                            <label for=""><b>Kesimpulan</b></label> <br>
                            <textarea name="kesimpulan_pengembangan" id="kesimpulanPengembangan" class="form-control"
                                >
                            </textarea>
                            <script>
                                tinymce.init({
                                    selector: '#kesimpulanPengembangan',
                                    height: 350,
                                    plugins: 'table',
                                    table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
                                    content_style: "body { font-family: Arial; }",
                                })
                            </script>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal -->

        <!-- Modal Ubah Laporan Pengembangan -->
        <div class="modal fade" id="ubahLaporanPengembangan" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Laporan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <form action="{{ url('tambah-pengembangan/'.$laporan_id) }}" method="POST">
                                @method('POST')
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="">Tanggal</label>
                                    <input name="tgl_pengembangan" class="form-control" type="date" id="inputTanggal"
                                        value="{{ $laporan->tgl_pengembangan }}" required>
                                </div>
                                <label for=""><b>Kesimpulan</b></label> <br>
                                <textarea name="kesimpulan_pengembangan" id="kesimpulanPengembangan2" class="form-control"
                                    required>
                                    {{ $laporan->kesimpulan_pengembangan }}
                                </textarea>
                                <script>
                                    tinymce.init({
                                        selector: '#kesimpulanPengembangan2',
                                        height: 350,
                                        plugins: 'table',
                                        table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
                                        content_style: "body { font-family: Arial; }",
                                    })
                                </script>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal -->


    <!-- TESTING -->

     <!-- Modal Tambah Data Fitur Testing-->
     <div class="modal fade" id="tambahFiturTesting" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nama Fitur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('tambah-fitur-testing') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="container1">
                            <div class="form-group-row">
                                <button type="button" class="btn btn-success btn-md add_form_field col-md-1 mb-2"
                                    id="tambah"><i class="fas fa-plus col-md-1 ml-0 pl-0"></i></button>
                                <input type="hidden" name="laporan_id" value="{{ $laporan_id }}">
                                <input type="text" class="form-control col-md-9 mb-3" name="nama_fitur[]">
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

    <!-- Modal Ubah Data Fitur Testing-->
    {{-- @foreach ($testing as $item)
        <div class="modal fade" id="ubahFiturTesting-{{ $item->id }}" tabindex="-1"
            aria-labelledby="modalUbahFitur" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ubah Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="ubah" action="{{ url('/update-fitur-testing/'. $item->id) }}" method="POST">
                            @method('POST')
                            {{ csrf_field() }}
                            <div class="container1">
                                <div class="form-group-row">
                                    <input type="hidden" name="laporan_id" value="{{ $laporan_id }}">
                                    <input type="text" class="form-control col-md-9 mb-3" name="nama_fitur"
                                        value="{{ $item->nama_fitur }}">
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal Ubah Fitur Testing-->

        <!-- Modal Hapus Fitur Testing-->
        <div class="modal fade" id="hapusFiturTesting-{{ $item->id }}" tabindex="-1" aria-labelledby="modalHapus"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalHapus">Hapus Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Apakah Anda yakin ingin menghapus?
                    </div>
                    <div class="modal-footer">
                        <form class="delete" action="{{ url('/hapus-fitur-testing/'.$item->id) }}" method="POST">
                            @method('POST')
                            <input type="hidden" name="laporan_id" value="{{ $laporan_id }}">
                            @csrf
                            <button class="btn btn-danger">Hapus
                            </button>
                        </form>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach --}}
    <!-- End Modal Hapus Data Fitur -->

    <!-- Modal Tambah Laporan Testing -->
    {{-- <div class="modal fade" id="tambahLaporanTesting" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Laporan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('tambah-testing/'.$laporan_id) }}" method="POST">
                        @method('POST')
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for=""><b>Tanggal</b></label>
                            <input name="tgl_testing" class="form-control" type="date" id="inputTanggal" required>
                        </div>
                        <label for=""><b>Kesimpulan</b></label> <br>
                        <textarea name="kesimpulan_testing" id="kesimpulanTesting" class="form-control"
                            required>
                        </textarea>
                        <script>
                            tinymce.init({
                                selector: '#kesimpulanTesting',
                                height: 350,
                                plugins: 'table',
                                table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
                                content_style: "body { font-family: Arial; }",
                            })
                        </script>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- End Modal -->

    <!-- Modal Ubah Laporan Testing -->
    {{-- <div class="modal fade" id="ubahLaporanTesting" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Laporan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body">
                        <form action="{{ url('tambah-testing/'.$laporan_id) }}" method="POST">
                            @method('POST')
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for=""><b>Tanggal</b></label>
                                <input name="tgl_testing" class="form-control" type="date" id="inputTanggal"
                                    value="{{ $laporan->tgl_testing }}" required>
                            </div>
                            <label for=""><b>Kesimpulan</b></label> <br>
                            <textarea name="kesimpulan_testing" id="kesimpulanTesting2" class="form-control"
                                required>
                                {{ $laporan->kesimpulan_testing }}
                            </textarea>
                            <script>
                                tinymce.init({
                                    selector: '#kesimpulanTesting2',
                                    height: 350,
                                    plugins: 'table',
                                    table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
                                    content_style: "body { font-family: Arial; }",
                                })
                            </script>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- End Modal -->


    <!-- RILIS -->

     <!-- Modal Tambah Data Fitur Rilis-->
     {{-- <div class="modal fade" id="tambahFiturRilis" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nama Fitur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('tambah-fitur-rilis') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="container1">
                            <div class="form-group-row">
                                <button type="button" class="btn btn-success btn-md add_form_field col-md-1 mb-2"
                                    id="tambah"><i class="fas fa-plus col-md-1 ml-0 pl-0"></i></button>
                                <input type="hidden" name="laporan_id" value="{{ $laporan_id }}">
                                <input type="text" class="form-control col-md-9 mb-3" name="nama_fitur[]">
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- End Modal -->

    <!-- Modal Ubah Data Fitur Rilis-->
    {{-- @foreach ($rilis as $item)
        <div class="modal fade" id="ubahFiturRilis-{{ $item->id }}" tabindex="-1"
            aria-labelledby="modalUbahFitur" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ubah Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="ubah" action="{{ url('/update-fitur-rilis/'. $item->id) }}" method="POST">
                            @method('POST')
                            {{ csrf_field() }}
                            <div class="container1">
                                <div class="form-group-row">
                                    <input type="hidden" name="laporan_id" value="{{ $laporan_id }}">
                                    <input type="text" class="form-control col-md-9 mb-3" name="nama_fitur"
                                        value="{{ $item->nama_fitur }}">
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal Ubah Fitur Rilis-->

        <!-- Modal Hapus Fitur Rilis-->
        <div class="modal fade" id="hapusFiturRilis-{{ $item->id }}" tabindex="-1" aria-labelledby="modalHapus"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalHapus">Hapus Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Apakah Anda yakin ingin menghapus?
                    </div>
                    <div class="modal-footer">
                        <form class="delete" action="{{ url('/hapus-fitur-rilis/'.$item->id) }}" method="POST">
                            @method('POST')
                            <input type="hidden" name="laporan_id" value="{{ $laporan_id }}">
                            @csrf
                            <button class="btn btn-danger">Hapus
                            </button>
                        </form>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach --}}
    <!-- End Modal Hapus Data Fitur -->

    <!-- Modal Tambah Laporan Rilis -->
    {{-- <div class="modal fade" id="tambahLaporanRilis" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Laporan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('tambah-rilis/'.$laporan_id) }}" method="POST">
                        @method('POST')
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for=""><b>Tanggal</b></label>
                            <input name="tgl_rilis" class="form-control" type="date" id="inputTanggal" required>
                        </div>
                        <label for=""><b>Kesimpulan</b></label> <br>
                        <textarea name="kesimpulan_rilis" id="kesimpulanRilis" class="form-control"
                            required>
                        </textarea>
                        <script>
                            tinymce.init({
                                selector: '#kesimpulanRilis',
                                height: 350,
                                plugins: 'table',
                                table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
                                content_style: "body { font-family: Arial; }",
                            })
                        </script>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- End Modal -->

    <!-- Modal Ubah Laporan Rilis -->
    {{-- <div class="modal fade" id="ubahLaporanRilis" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Laporan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-body">
                        <form action="{{ url('tambah-rilis/'.$laporan_id) }}" method="POST">
                            @method('POST')
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for=""><b>Tanggal</b></label>
                                <input name="tgl_rilis" class="form-control" type="date" id="inputTanggal"
                                    value="{{ $laporan->tgl_rilis }}" required>
                            </div>
                            <label for=""><b>Kesimpulan</b></label> <br>
                            <textarea name="kesimpulan_rilis" id="kesimpulanRilis2" class="form-control"
                                required>
                                {{ $laporan->kesimpulan_rilis }}
                            </textarea>
                            <script>
                                tinymce.init({
                                    selector: '#kesimpulanRilis2',
                                    height: 350,
                                    plugins: 'table',
                                    table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
                                    content_style: "body { font-family: Arial; }",
                                })
                            </script>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- End Modal -->


    </main>
    @include('sweetalert::alert')
@endsection

@section('footer')
    @include('layouts.partials.footer')
@endsection



<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"></script>


<script>
    $(document).ready(function() {
        // Action next
        $('.btn-next').on('click', function() {
            // Get value from data-to in button next
            const n = $(this).attr('data-to');
            // Action trigger click for tag a with id in value n
            $(n).trigger('click');
        });
        // Action back
        $('.btn-prev').on('click', function() {
            // Get value from data-to in button prev
            const n = $(this).attr('data-to');
            // Action trigger click for tag a with id in value n
            $(n).trigger('click');
        });

        // DYNAMIC FORM
        var max_fields = 100;
        var wrapper = $(".container1");
        var add_button = $(".add_form_field");

        var x = 1;
        $(add_button).click(function(e) {
            e.preventDefault();
            if (x < max_fields) {
                x++;
                $(wrapper).append(
                    '<div class="form-group row "><input class="form-control col-md-9 ml-3" type="text" name="nama_fitur[]"/><button type="button" class="delete btn btn-danger col-md-1 ml-3"><i class="far fa-trash-alt"></i></button></div>'
                ); //add input box
            } else {
                alert('Anda sudah mencapai batas limit')
            }
        });

        $(wrapper).on("click", ".delete", function(e) {
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        })

//         var input = document.querySelector('#clear');
// var textarea = document.querySelector('#output');

// input.addEventListener('click', function () {
//     textarea.value = '';
// }, false);


        // $('#form-tambah').on('submit', function(e) {
        //     e.preventDefault();
        //     console.log($(this).serialize());
        // })

        // $('.form-requirement').on('submit', function(e) {
        //     e.preventDefault();
        //     console.log($(this).serialize());
        // })

        // $('.form-pengembangan').on('submit', function(e) {
        //     e.preventDefault();
        //     console.log($(this).serialize());
        // })

        // $('.form-testing').on('submit', function(e) {
        //     e.preventDefault();
        //     console.log($(this).serialize());
        // })

        // $('.form-rilis').on('submit', function(e) {
        //     e.preventDefault();
        //     console.log($(this).serialize());
        // })

        // $('#').on('submit', function(e) {
        //     e.preventDefault();
        //     console.log($(this).serialize());
        // })



    });
    function eraseTextRequirement(name) {
    document.getElementsByName(name)[0].value = "";
    }
    function eraseTextPengembangan(name) {
    document.getElementsByName(name)[0].value = "";
    }
    function eraseTextTesting(name) {
    document.getElementsByName(name)[0].value = "";
    }
    function eraseTextRilis(name) {
    document.getElementsByName(name)[0].value = "";
    }
    
    
</script>
