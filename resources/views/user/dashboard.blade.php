@extends('layouts.master')

@section('title', 'Devbooks')
@section('topnav')
    @include('layouts.partials.topnav')
@endsection

@section('sidenav')
    @include('layouts.partials.sidenav')
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        
                {{-- <option value="{{$role->id}}">{{$role->name}}</option> --}}
        <div class="container-fluid">
            @if (Auth::user()->role_id == 2)
        {{-- <form action="{{url('create')}}" method="get">
            <div class="form-group">
               
                <label for="exampleFormControlSelect1">Pilih Tahapan</label>
                <select name="id_tahapan" id="" class="tahapan form-control" required>
                    <option value="">- Pilih -</option>
                    @foreach($tahapans as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endforeach
                </select> <br>
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form> --}}
        <!-- Button trigger modal -->
            <button type="button" class="btn btn-info mt-3" data-toggle="modal" data-target="#ModalTambah"><i class="fas fa-plus-circle mr-2"></i>
                Tambah
            </button>
            @endif
        <!-- End Button trigger modal -->
       

            <!-- Modal Tambah Data-->
            <div class="modal fade" id="ModalTambah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{url('tambah-laporan')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="inputLaporan">Nama Aplikasi</label>
                                <input name="nama_aplikasi" value="{{ old('nama_aplikasi') }}" type="text" class="form-control  @error('nama_aplikasi') is-invalid @enderror" id="inputNama" autofocus>
                                @error('nama_aplikasi')
                                    <div class="invalid-feedback">
                                        {{ $message}}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="inputVersi">Versi Aplikasi</label>
                                <div class="col-lg-12 col-xl-3 col-sm-4 pl-0">
                                    <input name="versi" type="number" min="1" class="form-control" id="inputVersi" step="any" step=".01" @error('versi') is-invalid @enderror required>
                                    @error('versi')
                                    <div class="invalid-feedback">
                                        {{ $message}}</div>
                                @enderror
                                </div>
                            </div>
                            {{-- <div class="form-group">
                                <label for="inputLaporan">Jenis</label>
                                <div class="form-check">
                                    <input name="jenis" class="form-check-input" type="radio" name="jenis" id="flexRadioDefault1" value="Pemeliharaan" @error('jenis') is-invalid @enderror required>
                                    <label class="form-check-label" for="pemeliharaan">
                                        Pemeliharaan
                                    </label>
                                    </div>
                                    <div class="form-check">
                                    <input name="jenis" class="form-check-input" type="radio" name="jenis" id="flexRadioDefault2" value="Perbaikan" @error('jenis') is-invalid @enderror required>
                                    <label class="formCheckLabel" for="perbaikan">
                                        Perbaikan
                                    </label>
                                    @error('jenis')
                                    <div class="invalid-feedback">
                                        {{ $message}}</div>
                                @enderror
                                </div>
                            </div>  --}}
                    </div>
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        </form>
                
                    </div>
                </div>
                </div>
            </div>
            <!-- End Modal Tambah Data -->

            {{-- Modal Ubah Data --}}
            @foreach ($laporan as $data )
            <div class="modal fade" id="modalUbah-{{ $data->id }}" tabindex="-1" aria-labelledby="modalUbah" aria-hidden="true">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ubah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <form class="ubah" action="{{url('/update-laporan/'.$data->id)}}" method="POST">
                            @method('PUT')
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="inputLaporan">Nama Aplikasi</label>
                                <input name="nama_aplikasi" type="text" class="form-control" id="inputNama" value="{{ old('nama_aplikasi', $data->nama_aplikasi) }}" required>
                            </div>
                            <div class="form-group">
                                <label for="inputLaporan">Versi Aplikasi</label>
                                    <input name="versi" type="number" class="form-control" id="inputVersi" value="{{ old('versi', $data->versi) }}" required>
                            </div>
                    </div>
                    
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        </form>
                    </div>
                </div>
                </div>
            </div>
            @endforeach
            {{-- End Modal Ubah Data --}}
        </div>

        {{-- Card --}}
        <div class="card my-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                Daftar Catatan Aplikasi
            </div>
            <div class="card-body">
                <div class="table-responsive glyphicon glyphicon-sort">
                <table id="example" class="table table-striped table-bordered glyphicon glyphicon-sort" style="width:100%">
        <thead>
            <tr>
                <th>No.</th>
                {{-- <th>Tanggal</th> --}}
                <th>Nama Aplikasi</th>
                <th>Versi</th>
                <th>Tahapan</th>
                <th>Posisi</th>
                <th>Status</th>
                <th>Aksi</th>
                
                
            </tr>
         
        </thead>
        <tbody>
            @foreach ($laporan as $datalaporan)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    {{-- <td>{{ \Carbon\Carbon::parse($datalaporan->tgl_laporan)->isoFormat('dddd, D MMMM Y')}}</td> --}}
                    <td>{{ $datalaporan->nama_aplikasi }}</td>
                    <td>{{ $datalaporan->versi }}</td>
                    @if($datalaporan->nama_tahapan == 'selesai')
                        <td><span class="badge badge-pill badge-success" style="padding: 7px; font-family: sans-serif; font-size: 9px">Selesai</span></td>
                    @else
                        <td>{{$datalaporan->nama_tahapan}}</td>
                    @endif
                    
                    <td>{{ $datalaporan->posisi }}</td>
                    <td>{{ $datalaporan->status }}</td>
                    <td>
                        {{-- <a class="btn btn-info btn-sm" href="{{ url('mengubah-laporan/'.$datalaporan->id.'/edit') }}" role="button"><i class="fas fa-edit"></i> Ubah</a> --}}
        
                        
                        <!-- @if ($datalaporan->status == 'Disetujui')
                        {{-- <a class="btn btn-success btn-sm" href="{{ url('generate/'.$datalaporan->id) }}" role="button"><i class="far fa-file-alt"></i> Hasil Generate</a>    --}}
                        @else
                        <a class="btn btn-success btn-sm disabled" href="{{ url('generate/'.$datalaporan->id) }}" role="button" aria-disabled="true"><i class="far fa-file-alt"></i> Hasil Generate</a>
                        @endif
                         -->
                        
                            <div class="btn-group mb-1">
                        <a class="btn btn-warning btn-sm mx-1 col-sm" href="{{ url('detail-laporan/'.$datalaporan->id) }}" role="button" aria-disabled="true"><i class="fas fa-info"></i> Detail</a>
                            </div>
   
                        <!-- Button trigger modal -->
                            @if (Auth::user()->role_id == 2)
                                
                                    {{-- <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModal"><i class="far fa-trash-alt"></i>
                                        Hapus
                                    </button> --}}
                                    <div class="btn-group">
                                    <a href="" type="button" class="btn btn-sm btn-success mx-1 col-sm" data-toggle="modal" data-target="#modalUbah-{{ $datalaporan->id }}"><i class="fas fa-edit "></i> Ubah
                                    </a>
                                 </div>
                                    
                                    <div class="btn-group">
                                    <a onclick="modalNotif({{$datalaporan->id}})" type="button" class="btn btn-sm btn-danger mx-1 col-sm" data-toggle="modal" data-target="#exampleModal"><i class="far fa-trash-alt"></i> Hapus</a>
                                </div>
                            @endif
                        
                            
                        {{-- <div class="btn-group">
                            Detail
                            </button>
                            <div class="dropdown-menu">
                                @foreach ($tahapans as $tahapan)
                                <a class="dropdown-item" value="{{$tahapan->id}}" href="{{ url('detail/'.$datalaporan->id) }}">{{$tahapan->nama}}</a>
                                @endforeach
                            </div>
                        </div>  --}}
                    </td>   
                </tr>
            @endforeach
            
        </tbody>
        
    </table>
                </div>
            </div>
        </div>
    </div>
</main>

<!-- Modal Hapus-->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Hapus Laporan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
            Apakah Anda yakin ingin menghapus?
            </div>
            <div class="modal-footer">
                <form class="delete" action="/"  method="POST">
                @method('delete')
                @csrf      
                <button class="btn btn-danger">Hapus
                </button>
                </form>  
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
@include('sweetalert::alert')
@endsection

@section('footer')
    @include('layouts.partials.footer')
@endsection

<script>
    function modalNotif(id) {
        $('.delete').attr('action', "/hapus-laporan/"+id);
    }
</script>