@extends('layouts.master')

@section('title', 'Dashboard Admin')
@section('topnav')
    @include('layouts.partials.topnav')
@endsection

@section('sidenav')
    @include('layouts.partials.sidenav')
@endsection
@section('content')
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">User Requirement</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('detail/'.$laporan->id) }}">Detail</a></li>
                            <li class="breadcrumb-item active">Ubah</li>
                        </ol>

                     <!-- Isi Form -->  
                        
                    <div class="card mt-4 mb-5">
                        <div class="card-header">
                            <i class="far fa-plus-square mr-1"></i>
                            Mengubah Laporan
                        </div>
                        <div class="card-body form-admin">
                            @if (Auth::user()->role_id == 2)
                                <form action="{{ url('subkoordinator/update/'.$laporan->id) }}" method="POST">
                            @elseif (Auth::user()->role_id == 3)
                                <form action="{{ url('koordinator/update/'.$laporan->id) }}" method="POST">
                            @elseif (Auth::user()->role_id == 4)
                                 <form action="{{ url('direktur/update/'.$laporan->id) }}" method="POST">
                            @endif
                            
                                @method('PUT')
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label for="tanggal" class="col-md-2 col-sm-12 col-form-label">Tanggal Pembuatan</label>
                                    <div class="col-auto">
                                        <input name="tgl_laporan" class="form-control" type="date" id="inputTanggal" value="{{ old('tgl_laporan', $laporan->tgl_laporan) }}" required>
                                    </div> 
                                </div>
                                <div class="form-grup row">
                                    <label for="versi" class="col-md-2 col-sm-12 col-form-label">Versi</label>
                                        <div class="col-lg-2 col-xl-1 col-sm-4">
                                            <input name="versi" class="form-control" type="text" id="versi" value="{{ old('versi', $laporan->versi) }}" required readonly>
                                        </div>
                                </div>
                                <br>
                                <div class="form-group row">
                                    <label for="judul" class="col-md-2 col-sm-12 col-form-label">Judul Laporan</label>
                                    <div class="col-md-6">
                                        <input name="judul" class="form-control" type="text" id="judul" value="{{ old('judul', $laporan->judul) }}" required >
                                    </div>  
                                </div>
                                <div class="form-group row">
                                    <label for="perihal" class="col-md-2 col-sm-12 col-form-label">Perihal</label>
                                    <div class="col-md-6">
                                        <input name="perihal" class="form-control" type="text" id="perihal" value="{{ old('perihal', $laporan->perihal) }}" required >
                                    </div>  
                                </div>
                                <div class="form-group row">
                                    <label for="FormControlSelect1" class="col-md-2 col-sm-12 col-form-label">Jenis Laporan</label>
                                    <div class="col-auto">      
                                        <div class="form-check">
                                            <input type="radio" class="form-check-input" name="jenis" value="Perbaikan" 
                                            <?php if ($laporan->jenis == "Perbaikan"): ?>
                                            checked="checked"
                                            <?php endif ?>> Perbaikan
                                        </div>
                                        <div class="form-check">
                                            <input type="radio" class="form-check-input" name="jenis" value="Pemeliharaan" 
                                            <?php if ($laporan->jenis == "Pemeliharaan"): ?>
                                            checked="checked"
                                            <?php endif ?> > Pemeliharaan

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="isiLaporan" class="col-md-2 col-sm-12 col-form-label">Isi Laporan</label>
                                        <div class="col-sm-12 col-md-8"> 
                                            <textarea name="isi" id="isiLaporan" class="form-control" required>{{ old('isi', $laporan->isi) }}
                                            </textarea>
                                            <script>
                                                tinymce.init({
                                                    selector: '#isiLaporan',
                                                    height: 350,
                                                    plugins: 'table',
                                                    table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol'
                                                   
                                                })
                                            </script>
                                        </div>
                                </div>
                                <div class="form-group row">
                                    <label for="kesimpulan" class="col-md-2 col-sm-12 col-form-label">Kesimpulan</label>
                                        <div class="col-sm-12 col-md-8"> 
                                            <textarea name="kesimpulan" id="kesimpulan" class="form-control" required>{{ old('kesimpulan', $laporan->kesimpulan) }}
                                            </textarea>
                                            <script>
                                                tinymce.init({
                                                    selector: '#kesimpulan',
                                                    height: 350,
                                                    plugins: 'table',
                                                    table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol'
                                                   
                                                })
                                            </script>
                                        </div>
                                </div>
                                {{-- <div class="form-group row">
                                    <label for="kesimpulan" class="col-md-2 col-sm-12 col-form-label">Kesimpulan/Saran</label>
                                    <div class="col-md-6">
                                        <textarea name="kesimpulan" id="kesimpulan" class="form-control" >{{ old('kesimpulan', $laporan->kesimpulan) }}</textarea>
                                    </div>  
                                </div>  --}}
                                <div class="row justify-content-sm-center mb-5">
                                    <button type="submit" class="btn btn-info mr-2"><i class="far fa-save"></i> Simpan</button>
                                </div>
                            </form>
                            <br><br>
                           

                        </div>
                    </div>
                        
                    </div>
                </main>
@endsection
@section('footer')
    @include('layouts.partials.footer')
@endsection