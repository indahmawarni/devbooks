<!DOCTYPE html>
<html>
<head>
	<title>Laporan</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	{{-- <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet"> --}}
	<style type="text/css">
		/* @font-face {
			font-family: 'Roboto-Regular';
			src : url('../fonts/Roboto-Regular.ttf');
		} */
			table tr td,
			table tr th{
				font-size: 9pt;
			}
			/* .container hr {
				color: #000;
			} */
			.content {
				/* margin-left: 45px;
				margin-right: 45px; */
				text-align: justify;
			}
			.footer {
				margin-left: 70%;
				margin-bottom: 20%;
			}
			body {
				font-family: 'Arial', sans-serif;
				color: #000;
				font-size: 14px;
			}
			
			.kop-surat {
				font-size: 14px;
			}
			img {
				width: 20px;
				height: 20px;
			}
			ol {
				font-size: 14px;
			}
	
		</style>

</head>
<body>
	
	<center>
		{{-- <img src="{{ asset('image/logolkpp.png') }}" alt=""> --}}
        <strong class="kop-surat">LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH <br> REPUBLIK INDONESIA</strong><br>
        Gedung LKPP - Kompleks Rasuna Epicentrum <br> 
        Jln. Epicentrum Tengah Lot 11 B, Jakarta Selatan 12940 <br>
        Telepon 021-2991-2450 | Faksimile 021-2991-2451 | <i>Website</i> : www.lkpp.go.id
	</center>
	
	<hr color:#000; width:95%; background-color:#000;>
	<center>
		<strong>Laporan Rilis</strong><br>
		<strong>Aplikasi {{ $laporan->nama_aplikasi }}</strong>
	</center>
	<br>
	<div class="content">
		<strong>A. Isi</strong>
		<p>Berdasarkan pengembangan aplikasi {{ $laporan->nama_aplikasi}}, fitur yang akan dirilis yaitu:</p>

		<ol>
		@foreach ($rilis as $item)
			<li>{{ $item->nama_fitur }}</li>
		@endforeach
		</ol>
		<br>
		@if($laporan->kesimpulan_rilis != null)
		<strong>B. Kesimpulan dan Saran</strong>
		<p>{!! $laporan->kesimpulan_rilis !!}</p>
		@endif
	</div>
	<div class="footer">
		<p>Jakarta
			<br>
			{{\Carbon\Carbon::parse($laporan->tgl_rilis)->isoFormat('dddd, D MMMM Y')}}
			<br>
			<br>
			<br>
			<br>
			<br>
			Penyusun, <br>{{ $laporan->subkoordinator->nama }}
			<br>
			{{-- Menyetujui,<br>
			{{$laporan->direktur->nama}}
			<br>
			{{$laporan->koordinator->nama}} --}}
		</p>
	</div>
	
</body>
</html>