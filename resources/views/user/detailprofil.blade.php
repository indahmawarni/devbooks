@extends('layouts.master')

@section('title', 'Devbooks')
@section('topnav')
    @include('layouts.partials.topnav')
@endsection

@section('sidenav')
    @include('layouts.partials.sidenav')
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Profil</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Detail</li>
        </ol>
        @if(session('sukses'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Berhasil!</strong>
                {{session('sukses')}} 
            </div> 
            @endif
        <div class="card my-4">
            <div class="card-header">
                Detail Profil
            </div>
            <div class="container">
              <div class="card-body">
                <center>
                  <i class="fas fa-user-circle"></i>
                </center>
                <br>
                <center>
                  <div class="bg-profil">
                    <div class="table-responsive">
                        <table class="table ">
                            <tbody class="profil">
                              <tr>
                                <th scope="col">Nama</th>
                                <td>: {{ $user->nama }} </td>
                              </tr>
                              <tr>
                                <th scope="col">Username</th>
                                <td>: {{ $user->username }}</td>
                              </tr>
                              <tr>
                                <th scope="col">Email</th>
                                <td>: {{ $user->email }}</td>
                              </tr>
                              <tr>
                                <th scope="col">Role</th>
                                <td>: {{ $user->role->name }}</td>
                              </tr>
                              <tr>
                                <th scope="col">Unit Kerja</th>
                                <td>: {{ $user->unitkerja['nama']}}</td>
                              </tr>
                              {{-- <tr>
                                <th scope="col">Direktorat</th>
                                <td>: {{ $user->direktorat }}</td>
                              </tr> --}}
                            </tbody>
                          </center>
                        </table>
                        <br>
                    </div>
                  </div>
                  <br>
                  <div class="row justify-content-sm-center mb-5">  
                    <a class="btn btn-info" href="{{ url('profil/edit/'.Auth::user()->id) }}" role="button"><i class="fas fa-edit"></i> Ubah</a>
                  </div>
              </div>
            </div>
        </div>
    </div>
</main>
@include('sweetalert::alert')
@endsection

@section('footer')
    @include('layouts.partials.footer')
@endsection