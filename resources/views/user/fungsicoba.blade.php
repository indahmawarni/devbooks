@extends('layouts.master')

@section('title', 'Devbooks')
@section('topnav')
    @include('layouts.partials.topnav')
@endsection

@section('sidenav')
    @include('layouts.partials.sidenav')
@endsection
@section('content')
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">User Requirement</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Membuat Laporan</li>
                        </ol>

                     <!-- Isi Form -->  
                        
                    <div class="card mt-4 mb-5">
                        <div class="card-header">
                            <i class="far fa-plus-square mr-1"></i>
                            Membuat Laporan
                        </div>
                        <div class="card-body form-admin">
                            <form action="{{ url('create')}}" method="POST">
                                @csrf
                                <div class="form-group row">
                                    <label for="tanggal" class="col-md-2 col-sm-12 col-form-label"><b>Tanggal<b></label>
                                        <input name="tgl_laporan" class="form-control" type="date" id="inputTanggal" required>
                                </div>
                                {{-- <input type="hidden" name="id_tahapan" value="{{$tahapanId}}"> --}}
                                <div class="form-group row">
                                    <label for="judul" class="col-md-2 col-sm-12 col-form-label"><b>Judul Laporan</b></label>
                                    <div class="col-md-6">
                                        <input name="judul" class="form-control" type="text" id="judul" required>
                                    </div>  
                                </div>
                                <div class="form-group row">
                                    <label for="judul" class="col-md-2 col-sm-12 col-form-label"><b>Perihal</b></label>
                                    <div class="col-md-6">
                                        <input name="perihal" class="form-control" type="text" id="judul" required>
                                    </div>  
                                </div>
                                <div class="form-grup row">
                                    <label for="versi" class="col-md-2 col-sm-12 col-form-label"><b>Versi</b></label>
                                        <div class="col-lg-2 col-xl-1 col-sm-4">
                                            <input name="versi" class="form-control" type="text" id="versi" required>
                                        </div>
                                </div>
                                <br>
                                <div class="form-group row">
                                    <label for="FormControlSelect1" class="col-md-2 col-sm-12 col-form-label"><b>Jenis Laporan</b></label>
                                    <div class="col-auto">
                                        <div class="form-check">
                                            <input name="jenis" class="form-check-input" type="radio" name="jenis" id="flexRadioDefault1" value="Pemeliharaan">
                                            <label class="form-check-label" for="pemeliharaan">
                                                Pemeliharaan
                                            </label>
                                            </div>
                                            <div class="form-check">
                                            <input name="jenis" class="form-check-input" type="radio" name="jenis" id="flexRadioDefault2" value="Perbaikan">
                                            <label class="formCheckLabel" for="perbaikan">
                                                Perbaikan
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="isiLaporan" class="col-md-2 col-sm-12 col-form-label"><b>Isi Laporan</b></label>
                                        <div class="col-sm-12 col-md-8"> 
                                            <textarea name="isi" id="isiLaporan" class="form-control" required>
                                            </textarea>
                                            <script>
                                                tinymce.init({
                                                    selector: '#isiLaporan',
                                                    height: 350,
                                                    plugins: 'table',
                                                    table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
                                                    content_style: "body { font-family: Arial; }",
                                                   
                                                })
                                            </script>
                                        </div>
                                </div>
                                <div class="form-group row">
                                    <label for="kesimpulan" class="col-md-2 col-sm-12 col-form-label"><b>Kesimpulan</b></label>
                                        <div class="col-sm-12 col-md-8"> 
                                            <textarea name="kesimpulan" id="kesimpulan" class="form-control" required>
                                            </textarea>
                                            <script>
                                                tinymce.init({
                                                    selector: '#kesimpulan',
                                                    height: 350,
                                                    plugins: 'table',
                                                    table_toolbar: 'tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
                                                    content_style: "body { font-family: Arial; }",
                                                   
                                                })
                                            </script>
                                        </div>
                                </div>
                                <div class="row justify-content-sm-center mb-5">
                                    <button type="submit" class="btn btn-info mr-2"><i class="far fa-save"></i> Simpan</button>
                                    {{-- <button type="submit" class="btn btn-info ml-2"><i class="fas fa-paper-plane"></i> Kirim ke Atasan</button> --}}
                                </div>
                               
                                {{-- <form action="{{route("logout")}}" method="POST">
                                    @csrf
                                    <button class="dropdown-item"><i class="fas fa-sign-out-alt"></i> Logout</button>
                                </form> --}}
                            </form>
                            <br><br>
                           

                        </div>
                    </div>
                        
                    </div>
                </main>
@endsection
@section('footer')
    @include('layouts.partials.footer')
@endsection
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

